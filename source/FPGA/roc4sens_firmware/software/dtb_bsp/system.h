/*
 * system.h - SOPC Builder system and BSP software package information
 *
 * Machine generated for CPU 'cpu' in SOPC Builder design 'dtb_system'
 * SOPC Builder design path: ../../dtb/dtb_system.sopcinfo
 *
 * Generated: Mon Jul 24 11:34:57 CEST 2017
 */

/*
 * DO NOT MODIFY THIS FILE
 *
 * Changing this file will have subtle consequences
 * which will almost certainly lead to a nonfunctioning
 * system. If you do modify this file, be aware that your
 * changes will be overwritten and lost when this file
 * is generated again.
 *
 * DO NOT MODIFY THIS FILE
 */

/*
 * License Agreement
 *
 * Copyright (c) 2008
 * Altera Corporation, San Jose, California, USA.
 * All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 *
 * This agreement shall be governed in all respects by the laws of the State
 * of California and by the laws of the United States of America.
 */

#ifndef __SYSTEM_H_
#define __SYSTEM_H_

/* Include definitions from linker script generator */
#include "linker.h"


/*
 * CPU configuration
 *
 */

#define ALT_CPU_ARCHITECTURE "altera_nios2_qsys"
#define ALT_CPU_BIG_ENDIAN 0
#define ALT_CPU_BREAK_ADDR 0x08000820
#define ALT_CPU_CPU_FREQ 75000000u
#define ALT_CPU_CPU_ID_SIZE 1
#define ALT_CPU_CPU_ID_VALUE 0x00000000
#define ALT_CPU_CPU_IMPLEMENTATION "fast"
#define ALT_CPU_DATA_ADDR_WIDTH 0x1c
#define ALT_CPU_DCACHE_LINE_SIZE 32
#define ALT_CPU_DCACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_DCACHE_SIZE 4096
#define ALT_CPU_EXCEPTION_ADDR 0x00000120
#define ALT_CPU_FLUSHDA_SUPPORTED
#define ALT_CPU_FREQ 75000000
#define ALT_CPU_HARDWARE_DIVIDE_PRESENT 1
#define ALT_CPU_HARDWARE_MULTIPLY_PRESENT 1
#define ALT_CPU_HARDWARE_MULX_PRESENT 0
#define ALT_CPU_HAS_DEBUG_CORE 1
#define ALT_CPU_HAS_DEBUG_STUB
#define ALT_CPU_HAS_JMPI_INSTRUCTION
#define ALT_CPU_ICACHE_LINE_SIZE 32
#define ALT_CPU_ICACHE_LINE_SIZE_LOG2 5
#define ALT_CPU_ICACHE_SIZE 4096
#define ALT_CPU_INITDA_SUPPORTED
#define ALT_CPU_INST_ADDR_WIDTH 0x1c
#define ALT_CPU_NAME "cpu"
#define ALT_CPU_NUM_OF_SHADOW_REG_SETS 0
#define ALT_CPU_RESET_ADDR 0x08000000


/*
 * CPU configuration (with legacy prefix - don't use these anymore)
 *
 */

#define NIOS2_BIG_ENDIAN 0
#define NIOS2_BREAK_ADDR 0x08000820
#define NIOS2_CPU_FREQ 75000000u
#define NIOS2_CPU_ID_SIZE 1
#define NIOS2_CPU_ID_VALUE 0x00000000
#define NIOS2_CPU_IMPLEMENTATION "fast"
#define NIOS2_DATA_ADDR_WIDTH 0x1c
#define NIOS2_DCACHE_LINE_SIZE 32
#define NIOS2_DCACHE_LINE_SIZE_LOG2 5
#define NIOS2_DCACHE_SIZE 4096
#define NIOS2_EXCEPTION_ADDR 0x00000120
#define NIOS2_FLUSHDA_SUPPORTED
#define NIOS2_HARDWARE_DIVIDE_PRESENT 1
#define NIOS2_HARDWARE_MULTIPLY_PRESENT 1
#define NIOS2_HARDWARE_MULX_PRESENT 0
#define NIOS2_HAS_DEBUG_CORE 1
#define NIOS2_HAS_DEBUG_STUB
#define NIOS2_HAS_JMPI_INSTRUCTION
#define NIOS2_ICACHE_LINE_SIZE 32
#define NIOS2_ICACHE_LINE_SIZE_LOG2 5
#define NIOS2_ICACHE_SIZE 4096
#define NIOS2_INITDA_SUPPORTED
#define NIOS2_INST_ADDR_WIDTH 0x1c
#define NIOS2_NUM_OF_SHADOW_REG_SETS 0
#define NIOS2_RESET_ADDR 0x08000000


/*
 * Define for each module class mastered by the CPU
 *
 */

#define __ADV3224
#define __ALTERA_AVALON_EPCS_FLASH_CONTROLLER
#define __ALTERA_AVALON_JTAG_UART
#define __ALTERA_AVALON_ONCHIP_MEMORY2
#define __ALTERA_AVALON_PIO
#define __ALTERA_AVALON_SGDMA
#define __ALTERA_AVALON_SYSID_QSYS
#define __ALTERA_AVALON_TIMER
#define __ALTERA_NIOS2_QSYS
#define __ALTMEMDDR2
#define __DAQ_DMA
#define __I2C_MASTER
#define __LVDS2LCDS
#define __PROBE
#define __ROC4SENS
#define __SPI_MASTER
#define __USB_FT232


/*
 * LCDS_io configuration
 *
 */

#define ALT_MODULE_CLASS_LCDS_io lvds2lcds
#define LCDS_IO_BASE 0x8100080
#define LCDS_IO_IRQ -1
#define LCDS_IO_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LCDS_IO_NAME "/dev/LCDS_io"
#define LCDS_IO_SPAN 1
#define LCDS_IO_TYPE "lvds2lcds"


/*
 * LED configuration
 *
 */

#define ALT_MODULE_CLASS_LED altera_avalon_pio
#define LED_BASE 0x8100040
#define LED_BIT_CLEARING_EDGE_REGISTER 0
#define LED_BIT_MODIFYING_OUTPUT_REGISTER 0
#define LED_CAPTURE 0
#define LED_DATA_WIDTH 4
#define LED_DO_TEST_BENCH_WIRING 0
#define LED_DRIVEN_SIM_VALUE 0
#define LED_EDGE_TYPE "NONE"
#define LED_FREQ 75000000
#define LED_HAS_IN 0
#define LED_HAS_OUT 1
#define LED_HAS_TRI 0
#define LED_IRQ -1
#define LED_IRQ_INTERRUPT_CONTROLLER_ID -1
#define LED_IRQ_TYPE "NONE"
#define LED_NAME "/dev/LED"
#define LED_RESET_VALUE 0
#define LED_SPAN 16
#define LED_TYPE "altera_avalon_pio"


/*
 * SDC_SPI2 configuration
 *
 */

#define ALT_MODULE_CLASS_SDC_SPI2 spi_master
#define SDC_SPI2_BASE 0x8100160
#define SDC_SPI2_IRQ -1
#define SDC_SPI2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SDC_SPI2_NAME "/dev/SDC_SPI2"
#define SDC_SPI2_SPAN 16
#define SDC_SPI2_TYPE "spi_master"


/*
 * System configuration
 *
 */

#define ALT_DEVICE_FAMILY "Cyclone III"
#define ALT_ENHANCED_INTERRUPT_API_PRESENT
#define ALT_IRQ_BASE NULL
#define ALT_LOG_PORT "/dev/null"
#define ALT_LOG_PORT_BASE 0x0
#define ALT_LOG_PORT_DEV null
#define ALT_LOG_PORT_TYPE ""
#define ALT_NUM_EXTERNAL_INTERRUPT_CONTROLLERS 0
#define ALT_NUM_INTERNAL_INTERRUPT_CONTROLLERS 1
#define ALT_NUM_INTERRUPT_CONTROLLERS 1
#define ALT_STDERR "/dev/jtag_uart"
#define ALT_STDERR_BASE 0x8100180
#define ALT_STDERR_DEV jtag_uart
#define ALT_STDERR_IS_JTAG_UART
#define ALT_STDERR_PRESENT
#define ALT_STDERR_TYPE "altera_avalon_jtag_uart"
#define ALT_STDIN "/dev/jtag_uart"
#define ALT_STDIN_BASE 0x8100180
#define ALT_STDIN_DEV jtag_uart
#define ALT_STDIN_IS_JTAG_UART
#define ALT_STDIN_PRESENT
#define ALT_STDIN_TYPE "altera_avalon_jtag_uart"
#define ALT_STDOUT "/dev/jtag_uart"
#define ALT_STDOUT_BASE 0x8100180
#define ALT_STDOUT_DEV jtag_uart
#define ALT_STDOUT_IS_JTAG_UART
#define ALT_STDOUT_PRESENT
#define ALT_STDOUT_TYPE "altera_avalon_jtag_uart"
#define ALT_SYSTEM_NAME "dtb_system"


/*
 * USB2 configuration
 *
 */

#define ALT_MODULE_CLASS_USB2 usb_ft232
#define USB2_BASE 0x8001110
#define USB2_IRQ -1
#define USB2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define USB2_NAME "/dev/USB2"
#define USB2_SPAN 2
#define USB2_TYPE "usb_ft232"


/*
 * button configuration
 *
 */

#define ALT_MODULE_CLASS_button altera_avalon_pio
#define BUTTON_BASE 0x8100060
#define BUTTON_BIT_CLEARING_EDGE_REGISTER 0
#define BUTTON_BIT_MODIFYING_OUTPUT_REGISTER 0
#define BUTTON_CAPTURE 1
#define BUTTON_DATA_WIDTH 2
#define BUTTON_DO_TEST_BENCH_WIRING 0
#define BUTTON_DRIVEN_SIM_VALUE 0
#define BUTTON_EDGE_TYPE "ANY"
#define BUTTON_FREQ 75000000
#define BUTTON_HAS_IN 1
#define BUTTON_HAS_OUT 0
#define BUTTON_HAS_TRI 0
#define BUTTON_IRQ 9
#define BUTTON_IRQ_INTERRUPT_CONTROLLER_ID 0
#define BUTTON_IRQ_TYPE "EDGE"
#define BUTTON_NAME "/dev/button"
#define BUTTON_RESET_VALUE 0
#define BUTTON_SPAN 16
#define BUTTON_TYPE "altera_avalon_pio"


/*
 * crosspoint_switch configuration
 *
 */

#define ALT_MODULE_CLASS_crosspoint_switch adv3224
#define CROSSPOINT_SWITCH_BASE 0x81000a0
#define CROSSPOINT_SWITCH_IRQ -1
#define CROSSPOINT_SWITCH_IRQ_INTERRUPT_CONTROLLER_ID -1
#define CROSSPOINT_SWITCH_NAME "/dev/crosspoint_switch"
#define CROSSPOINT_SWITCH_SPAN 8
#define CROSSPOINT_SWITCH_TYPE "adv3224"


/*
 * daq_dma_0 configuration
 *
 */

#define ALT_MODULE_CLASS_daq_dma_0 daq_dma
#define DAQ_DMA_0_BASE 0x8200000
#define DAQ_DMA_0_IRQ -1
#define DAQ_DMA_0_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DAQ_DMA_0_NAME "/dev/daq_dma_0"
#define DAQ_DMA_0_SPAN 32
#define DAQ_DMA_0_TYPE "daq_dma"


/*
 * descriptor_memory configuration
 *
 */

#define ALT_MODULE_CLASS_descriptor_memory altera_avalon_onchip_memory2
#define DESCRIPTOR_MEMORY_ALLOW_IN_SYSTEM_MEMORY_CONTENT_EDITOR 0
#define DESCRIPTOR_MEMORY_ALLOW_MRAM_SIM_CONTENTS_ONLY_FILE 0
#define DESCRIPTOR_MEMORY_BASE 0x8402000
#define DESCRIPTOR_MEMORY_CONTENTS_INFO ""
#define DESCRIPTOR_MEMORY_DUAL_PORT 0
#define DESCRIPTOR_MEMORY_GUI_RAM_BLOCK_TYPE "AUTO"
#define DESCRIPTOR_MEMORY_INIT_CONTENTS_FILE "dtb_system_descriptor_memory"
#define DESCRIPTOR_MEMORY_INIT_MEM_CONTENT 1
#define DESCRIPTOR_MEMORY_INSTANCE_ID "NONE"
#define DESCRIPTOR_MEMORY_IRQ -1
#define DESCRIPTOR_MEMORY_IRQ_INTERRUPT_CONTROLLER_ID -1
#define DESCRIPTOR_MEMORY_NAME "/dev/descriptor_memory"
#define DESCRIPTOR_MEMORY_NON_DEFAULT_INIT_FILE_ENABLED 0
#define DESCRIPTOR_MEMORY_RAM_BLOCK_TYPE "AUTO"
#define DESCRIPTOR_MEMORY_READ_DURING_WRITE_MODE "DONT_CARE"
#define DESCRIPTOR_MEMORY_SINGLE_CLOCK_OP 0
#define DESCRIPTOR_MEMORY_SIZE_MULTIPLE 1
#define DESCRIPTOR_MEMORY_SIZE_VALUE 256
#define DESCRIPTOR_MEMORY_SPAN 256
#define DESCRIPTOR_MEMORY_TYPE "altera_avalon_onchip_memory2"
#define DESCRIPTOR_MEMORY_WRITABLE 1


/*
 * epcs_controller configuration
 *
 */

#define ALT_MODULE_CLASS_epcs_controller altera_avalon_epcs_flash_controller
#define EPCS_CONTROLLER_BASE 0x8000000
#define EPCS_CONTROLLER_IRQ 6
#define EPCS_CONTROLLER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define EPCS_CONTROLLER_NAME "/dev/epcs_controller"
#define EPCS_CONTROLLER_REGISTER_OFFSET 1024
#define EPCS_CONTROLLER_SPAN 2048
#define EPCS_CONTROLLER_TYPE "altera_avalon_epcs_flash_controller"


/*
 * hal configuration
 *
 */

#define ALT_MAX_FD 32
#define ALT_SYS_CLK SYS_TIMER
#define ALT_TIMESTAMP_CLK none


/*
 * i2c_external configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_external i2c_master
#define I2C_EXTERNAL_BASE 0x8100120
#define I2C_EXTERNAL_IRQ 8
#define I2C_EXTERNAL_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_EXTERNAL_NAME "/dev/i2c_external"
#define I2C_EXTERNAL_SPAN 8
#define I2C_EXTERNAL_TYPE "i2c_master"


/*
 * i2c_main configuration
 *
 */

#define ALT_MODULE_CLASS_i2c_main i2c_master
#define I2C_MAIN_BASE 0x8100100
#define I2C_MAIN_IRQ 7
#define I2C_MAIN_IRQ_INTERRUPT_CONTROLLER_ID 0
#define I2C_MAIN_NAME "/dev/i2c_main"
#define I2C_MAIN_SPAN 8
#define I2C_MAIN_TYPE "i2c_master"


/*
 * jtag_uart configuration
 *
 */

#define ALT_MODULE_CLASS_jtag_uart altera_avalon_jtag_uart
#define JTAG_UART_BASE 0x8100180
#define JTAG_UART_IRQ 10
#define JTAG_UART_IRQ_INTERRUPT_CONTROLLER_ID 0
#define JTAG_UART_NAME "/dev/jtag_uart"
#define JTAG_UART_READ_DEPTH 64
#define JTAG_UART_READ_THRESHOLD 8
#define JTAG_UART_SPAN 8
#define JTAG_UART_TYPE "altera_avalon_jtag_uart"
#define JTAG_UART_WRITE_DEPTH 64
#define JTAG_UART_WRITE_THRESHOLD 8


/*
 * main_control configuration
 *
 */

#define ALT_MODULE_CLASS_main_control altera_avalon_pio
#define MAIN_CONTROL_BASE 0x81000c0
#define MAIN_CONTROL_BIT_CLEARING_EDGE_REGISTER 0
#define MAIN_CONTROL_BIT_MODIFYING_OUTPUT_REGISTER 0
#define MAIN_CONTROL_CAPTURE 0
#define MAIN_CONTROL_DATA_WIDTH 9
#define MAIN_CONTROL_DO_TEST_BENCH_WIRING 0
#define MAIN_CONTROL_DRIVEN_SIM_VALUE 0
#define MAIN_CONTROL_EDGE_TYPE "NONE"
#define MAIN_CONTROL_FREQ 75000000
#define MAIN_CONTROL_HAS_IN 0
#define MAIN_CONTROL_HAS_OUT 1
#define MAIN_CONTROL_HAS_TRI 0
#define MAIN_CONTROL_IRQ -1
#define MAIN_CONTROL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MAIN_CONTROL_IRQ_TYPE "NONE"
#define MAIN_CONTROL_NAME "/dev/main_control"
#define MAIN_CONTROL_RESET_VALUE 0
#define MAIN_CONTROL_SPAN 16
#define MAIN_CONTROL_TYPE "altera_avalon_pio"


/*
 * main_status configuration
 *
 */

#define ALT_MODULE_CLASS_main_status altera_avalon_pio
#define MAIN_STATUS_BASE 0x81000e0
#define MAIN_STATUS_BIT_CLEARING_EDGE_REGISTER 0
#define MAIN_STATUS_BIT_MODIFYING_OUTPUT_REGISTER 0
#define MAIN_STATUS_CAPTURE 0
#define MAIN_STATUS_DATA_WIDTH 7
#define MAIN_STATUS_DO_TEST_BENCH_WIRING 0
#define MAIN_STATUS_DRIVEN_SIM_VALUE 0
#define MAIN_STATUS_EDGE_TYPE "NONE"
#define MAIN_STATUS_FREQ 75000000
#define MAIN_STATUS_HAS_IN 1
#define MAIN_STATUS_HAS_OUT 0
#define MAIN_STATUS_HAS_TRI 0
#define MAIN_STATUS_IRQ -1
#define MAIN_STATUS_IRQ_INTERRUPT_CONTROLLER_ID -1
#define MAIN_STATUS_IRQ_TYPE "NONE"
#define MAIN_STATUS_NAME "/dev/main_status"
#define MAIN_STATUS_RESET_VALUE 0
#define MAIN_STATUS_SPAN 16
#define MAIN_STATUS_TYPE "altera_avalon_pio"


/*
 * probe_d1 configuration
 *
 */

#define ALT_MODULE_CLASS_probe_d1 probe
#define PROBE_D1_BASE 0x8108800
#define PROBE_D1_IRQ -1
#define PROBE_D1_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PROBE_D1_NAME "/dev/probe_d1"
#define PROBE_D1_SPAN 1
#define PROBE_D1_TYPE "probe"


/*
 * probe_d2 configuration
 *
 */

#define ALT_MODULE_CLASS_probe_d2 probe
#define PROBE_D2_BASE 0x8108810
#define PROBE_D2_IRQ -1
#define PROBE_D2_IRQ_INTERRUPT_CONTROLLER_ID -1
#define PROBE_D2_NAME "/dev/probe_d2"
#define PROBE_D2_SPAN 1
#define PROBE_D2_TYPE "probe"


/*
 * ram_ext configuration
 *
 */

#define ALT_MODULE_CLASS_ram_ext altmemddr2
#define RAM_EXT_BASE 0x0
#define RAM_EXT_IRQ -1
#define RAM_EXT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define RAM_EXT_NAME "/dev/ram_ext"
#define RAM_EXT_SPAN 134217728
#define RAM_EXT_TYPE "altmemddr2"


/*
 * ram_ext configuration as viewed by usb_tx_dma_m_read
 *
 */

#define USB_TX_DMA_M_READ_RAM_EXT_BASE 0x0
#define USB_TX_DMA_M_READ_RAM_EXT_IRQ -1
#define USB_TX_DMA_M_READ_RAM_EXT_IRQ_INTERRUPT_CONTROLLER_ID -1
#define USB_TX_DMA_M_READ_RAM_EXT_NAME "/dev/ram_ext"
#define USB_TX_DMA_M_READ_RAM_EXT_SPAN 134217728
#define USB_TX_DMA_M_READ_RAM_EXT_TYPE "altmemddr2"


/*
 * roc4sens_ctrl configuration
 *
 */

#define ALT_MODULE_CLASS_roc4sens_ctrl roc4sens
#define ROC4SENS_CTRL_BASE 0x8108000
#define ROC4SENS_CTRL_IRQ -1
#define ROC4SENS_CTRL_IRQ_INTERRUPT_CONTROLLER_ID -1
#define ROC4SENS_CTRL_NAME "/dev/roc4sens_ctrl"
#define ROC4SENS_CTRL_SPAN 2048
#define ROC4SENS_CTRL_TYPE "roc4sens"


/*
 * sys_timer configuration
 *
 */

#define ALT_MODULE_CLASS_sys_timer altera_avalon_timer
#define SYS_TIMER_ALWAYS_RUN 0
#define SYS_TIMER_BASE 0x8100020
#define SYS_TIMER_COUNTER_SIZE 32
#define SYS_TIMER_FIXED_PERIOD 0
#define SYS_TIMER_FREQ 75000000
#define SYS_TIMER_IRQ 4
#define SYS_TIMER_IRQ_INTERRUPT_CONTROLLER_ID 0
#define SYS_TIMER_LOAD_VALUE 749999
#define SYS_TIMER_MULT 0.0010
#define SYS_TIMER_NAME "/dev/sys_timer"
#define SYS_TIMER_PERIOD 10
#define SYS_TIMER_PERIOD_UNITS "ms"
#define SYS_TIMER_RESET_OUTPUT 0
#define SYS_TIMER_SNAPSHOT 1
#define SYS_TIMER_SPAN 32
#define SYS_TIMER_TICKS_PER_SEC 100.0
#define SYS_TIMER_TIMEOUT_PULSE_OUTPUT 0
#define SYS_TIMER_TYPE "altera_avalon_timer"


/*
 * sysid configuration
 *
 */

#define ALT_MODULE_CLASS_sysid altera_avalon_sysid_qsys
#define SYSID_BASE 0x8100000
#define SYSID_ID 2
#define SYSID_IRQ -1
#define SYSID_IRQ_INTERRUPT_CONTROLLER_ID -1
#define SYSID_NAME "/dev/sysid"
#define SYSID_SPAN 8
#define SYSID_TIMESTAMP 1500876957
#define SYSID_TYPE "altera_avalon_sysid_qsys"


/*
 * usb_tx_dma configuration
 *
 */

#define ALT_MODULE_CLASS_usb_tx_dma altera_avalon_sgdma
#define USB_TX_DMA_ADDRESS_WIDTH 32
#define USB_TX_DMA_ALWAYS_DO_MAX_BURST 1
#define USB_TX_DMA_ATLANTIC_CHANNEL_DATA_WIDTH 4
#define USB_TX_DMA_AVALON_MM_BYTE_REORDER_MODE 0
#define USB_TX_DMA_BASE 0x8001040
#define USB_TX_DMA_BURST_DATA_WIDTH 8
#define USB_TX_DMA_BURST_TRANSFER 1
#define USB_TX_DMA_BYTES_TO_TRANSFER_DATA_WIDTH 16
#define USB_TX_DMA_CHAIN_WRITEBACK_DATA_WIDTH 32
#define USB_TX_DMA_COMMAND_FIFO_DATA_WIDTH 104
#define USB_TX_DMA_CONTROL_DATA_WIDTH 8
#define USB_TX_DMA_CONTROL_SLAVE_ADDRESS_WIDTH 0x4
#define USB_TX_DMA_CONTROL_SLAVE_DATA_WIDTH 32
#define USB_TX_DMA_DESCRIPTOR_READ_BURST 1
#define USB_TX_DMA_DESC_DATA_WIDTH 32
#define USB_TX_DMA_HAS_READ_BLOCK 1
#define USB_TX_DMA_HAS_WRITE_BLOCK 0
#define USB_TX_DMA_IN_ERROR_WIDTH 0
#define USB_TX_DMA_IRQ 5
#define USB_TX_DMA_IRQ_INTERRUPT_CONTROLLER_ID 0
#define USB_TX_DMA_NAME "/dev/usb_tx_dma"
#define USB_TX_DMA_OUT_ERROR_WIDTH 0
#define USB_TX_DMA_READ_BLOCK_DATA_WIDTH 16
#define USB_TX_DMA_READ_BURSTCOUNT_WIDTH 4
#define USB_TX_DMA_SPAN 64
#define USB_TX_DMA_STATUS_TOKEN_DATA_WIDTH 24
#define USB_TX_DMA_STREAM_DATA_WIDTH 16
#define USB_TX_DMA_SYMBOLS_PER_BEAT 2
#define USB_TX_DMA_TYPE "altera_avalon_sgdma"
#define USB_TX_DMA_UNALIGNED_TRANSFER 0
#define USB_TX_DMA_WRITE_BLOCK_DATA_WIDTH 16
#define USB_TX_DMA_WRITE_BURSTCOUNT_WIDTH 4

#endif /* __SYSTEM_H_ */
