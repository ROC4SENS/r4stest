// pixel_atb.h

#pragma once

#include "dtb_hal.h"
#include "rpc.h"
#include "FlashMemory.h"




template <class T> class HWvector;


class CTestboard
{
	CRpcIo *rpc_io;
	CUSB usb;

	static const uint16_t flashUpgradeVersion;
	uint16_t ugRecordCounter;
	CFlashMemory *flashMem;

	uint32_t mainCtrl;
	bool isPowerOn;

	uint16_t vd, va;
	uint16_t id, ia;

	uint16_t r4s_vcal;
	uint16_t r4s_rgpr;
	uint16_t r4s_rgsh;
	uint16_t r4s_vref;
	uint16_t r4s_vaux1;
	uint16_t r4s_vaux2;
	uint16_t r4s_vaux3;


	uint8_t ledstatus;

	uint8_t sig_level_clk;
	uint8_t sig_level_ctr;
	uint8_t sig_level_sda;
	uint8_t sig_level_tin;
	uint8_t sig_offset;

	// --- DAQ variables
	uint16_t *daq_mem_base; // DAQ buffer base address (0 = no space reserved)
	uint32_t daq_mem_size;  // DAQ buffer size in 16 bit words
	uint16_t daq_fifo_state;


	void InitDac();      // Power DAC (internal)
	void InitDacR4S0();  // ROC4Sens DAC (main)
	void InitDacR4S1();  // ROC4Send DAC (aux)

	void SetDac(int addr, int value);
	void SetDacR4S0(int addr, int value);
	void SetDacR4S1(int addr, int value);


	unsigned int ReadADC(unsigned char addr);
	unsigned int mV_to_DAC(int mV);
	unsigned int uA100_to_DAC(int ua100);
	int ADC_to_mV(unsigned int dac);
	int ADC_to_uA100(unsigned int dac);

	void Sig_Off();
	void Sig_Restore();

	static unsigned char COLCODE(unsigned char x) { return ((x>>1) & 0x7e)^x; }
	static unsigned char ROWCODE(unsigned char x) { return (x>>1)^x; }

public:
	CTestboard();
	CRpcIo* GetIo() { return rpc_io; }


	// === RPC ==============================================================

	RPC_EXPORT uint16_t GetRpcVersion();
	RPC_EXPORT int32_t  GetRpcCallId(string &cmdName);

	RPC_EXPORT void GetRpcTimestamp(stringR &ts);

	RPC_EXPORT int32_t GetRpcCallCount();
	RPC_EXPORT bool    GetRpcCallName(int32_t id, stringR &callName);


	// === DTB identification ===============================================

	RPC_EXPORT void GetInfo(stringR &info);
	RPC_EXPORT uint16_t GetBoardId();	// reads the board number

	RPC_EXPORT void GetHWVersion(stringR &version);
	RPC_EXPORT uint16_t GetFWVersion();
	RPC_EXPORT uint16_t GetSWVersion();


	// === DTB service ======================================================

	// --- upgrade
	RPC_EXPORT uint16_t UpgradeGetVersion();
	RPC_EXPORT uint8_t  UpgradeStart(uint16_t version);
	RPC_EXPORT uint8_t  UpgradeData(string &record);
	RPC_EXPORT uint8_t  UpgradeError();
	RPC_EXPORT void     UpgradeErrorMsg(stringR &msg);
	RPC_EXPORT void     UpgradeExec(uint16_t recordCount);

//	RPC_EXPORT(service) void Bootstrap();


	// === DTB functions ====================================================

	RPC_EXPORT void Init();

	RPC_EXPORT void Welcome();
	RPC_EXPORT void SetLed(uint8_t x);
	void ToggleLed(uint8_t x, bool on = true);


	// === Clock, Timing ====================================================

	RPC_EXPORT void cDelay(uint16_t clocks);
	RPC_EXPORT void uDelay(uint16_t us);
	void mDelay(uint16_t ms);


	// --- select ROC/Module clock source
	#define CLK_SRC_INT  0
	#define CLK_SRC_EXT  1
	RPC_EXPORT void SetClockSource(uint8_t source);

	// --- check if external clock is present
	RPC_EXPORT bool IsClockPresent();


	#define SIG_CLK 0
	#define SIG_CTR 1
	#define SIG_SDA 2
	#define SIG_TIN 3

	RPC_EXPORT void Sig_SetLevel(uint8_t signal, uint8_t level);
	RPC_EXPORT void Sig_SetOffset(uint8_t offset);
	RPC_EXPORT void Sig_SetLVDS();
	RPC_EXPORT void Sig_SetLCDS();


	// --- digital signal probe ---------------------------------------------
	#define PROBE_OFF             0
	#define PROBE_RUNNING         1
	#define PROBE_START_SEQ       2
	#define PROBE_START_CALPIX    3
	#define PROBE_START_MEASURE   4
	#define PROBE_START_READOUT   5
	#define PROBE_LA              6
	#define PROBE_CAL_ENA         7
	#define PROBE_CAL_PULSE       8


	RPC_EXPORT void SignalProbeD1(uint8_t signal);
	RPC_EXPORT void SignalProbeD2(uint8_t signal);


	// --- analog signal probe ----------------------------------------------
	#define PROBEA_RBI     0
	#define PROBEA_SDATA1  1
	#define PROBEA_SDATA2  2
	#define PROBEA_PHI1    3
	#define PROBEA_PHI2    4
	#define PROBEA_HOLD    5
	#define PROBEA_RBO     6
	#define PROBEA_OFF     7

	#define GAIN_1   0
	#define GAIN_2   1
	#define GAIN_3   2
	#define GAIN_4   3


	RPC_EXPORT void SignalProbeA1(uint8_t signal);
	RPC_EXPORT void SignalProbeA2(uint8_t signal);
	RPC_EXPORT void SignalProbeADC(uint8_t signal, uint8_t gain = 0);


	// --- ROC/Module power VD/VA -------------------------------------------
	RPC_EXPORT void Pon();
	RPC_EXPORT void Poff();

	RPC_EXPORT void _SetVD(uint16_t mV);
	RPC_EXPORT void _SetVA(uint16_t mV);
	RPC_EXPORT void _SetID(uint16_t uA100);
	RPC_EXPORT void _SetIA(uint16_t uA100);

	RPC_EXPORT uint16_t _GetVD();
	RPC_EXPORT uint16_t _GetVA();
	RPC_EXPORT uint16_t _GetID();
	RPC_EXPORT uint16_t _GetIA();

	RPC_EXPORT uint16_t _GetVD_Reg();
	RPC_EXPORT uint16_t _GetVDAC_Reg();
	RPC_EXPORT uint16_t _GetVD_Cap();

	RPC_EXPORT void HVon();
	RPC_EXPORT void HVoff();

	RPC_EXPORT uint8_t GetStatus();


	// --- data aquisition --------------------------------------------------
	RPC_EXPORT uint32_t Daq_Open(uint32_t buffersize = 10000000);
	RPC_EXPORT void Daq_Close();
	RPC_EXPORT void Daq_Start();
	RPC_EXPORT void Daq_Stop();
	RPC_EXPORT uint32_t Daq_GetSize();

	uint8_t Daq_Read(vectorR<uint16_t> &data,
			 uint32_t blocksize = 65536);

	uint8_t Daq_Read(vectorR<uint16_t> &data,
			uint32_t blocksize, uint32_t &availsize);

	RPC_EXPORT uint8_t Daq_Read(HWvectorR<uint16_t> &data,
			uint32_t blocksize = 65536);

	RPC_EXPORT uint8_t Daq_Read(HWvectorR<uint16_t> &data,
			uint32_t blocksize, uint32_t &availsize);


	// Ethernet test functions
	bool Ethernet_Init();
	RPC_EXPORT void Ethernet_Send(string &message);
	RPC_EXPORT uint32_t Ethernet_RecvPackets();

	// --- ROC4Sens adapter test functions ----------------------------------
	RPC_EXPORT void r4s_SetHoldPos(uint8_t t);
	RPC_EXPORT void r4s_AdcDelay(uint8_t t);
	RPC_EXPORT void r4s_SetSequence(vector<uint32_t> &prog);

//	RPC_EXPORT void r4s_SetPixCal(uint8_t x, uint8_t y);
	RPC_EXPORT void r4s_SetRegX(vector<uint32_t> &shr_x);
	RPC_EXPORT void r4s_SetRegY(vector<uint32_t> &shr_y);

	RPC_EXPORT void r4s_Enable(uint8_t t);
	RPC_EXPORT void r4s_Start();
	RPC_EXPORT void r4s_Stop();
	RPC_EXPORT bool r4s_Running();


	RPC_EXPORT void r4s_SetVcal(uint16_t mV);
	RPC_EXPORT void r4s_SetRgsh(uint16_t mV);
	RPC_EXPORT void r4s_SetRgpr(uint16_t mV);
	RPC_EXPORT void r4s_SetVref(uint16_t mV);
	RPC_EXPORT void r4s_SetVaux1(uint16_t mV);
	RPC_EXPORT void r4s_SetVaux2(uint16_t mV);
	RPC_EXPORT void r4s_SetVaux3(uint16_t mV);


	void r4s_Voff();
	void r4s_Von();

	uint16_t mV2DAC(uint16_t mV);
	void R4S_SetDAC(uint8_t channel, uint16_t value);
	void R4S_SetReg(uint8_t reg, uint32_t value);
};


extern CTestboard tb;

template <class T>
class HWvector
{
	uint16_t *p1;
	uint32_t s1;
	uint16_t *p2;
	uint32_t s2;

	uint32_t base;
	uint32_t rp;
public:
	HWvector() {}
	~HWvector() {}
	void Write(rpcMessage &msg, uint32_t &hdr);
	friend class CTestboard;
};

template <class T>
void HWvector<T>::Write(rpcMessage &msg, uint32_t &hdr)
{
	uint32_t size = (s1 + s2)*sizeof(uint16_t);
	hdr = (size << 8) + RPC_TYPE_DTB_DATA;
	msg.GetIo().Write(&hdr, sizeof(uint32_t));
	if (s1) msg.GetIo().Write(p1, s1*sizeof(uint16_t));
	if (s2)	msg.GetIo().Write(p2, s2*sizeof(uint16_t));
}
