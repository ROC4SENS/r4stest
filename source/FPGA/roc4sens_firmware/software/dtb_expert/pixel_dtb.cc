// pixel_dtb.cpp

#include "dtb_hal.h"
#include "pixel_dtb.h"
#include "dtb_config.h"
#include "rpc.h"
#include "SRecordReader.h"
#include "sys/alt_cache.h"



const int daq_read_size = 32768; // read sizedaq_read_size = 32768;


// === DTB identification ===================================================

uint16_t CTestboard::GetRpcVersion()
{
	return rpc_GetRpcVersion();
}


int32_t  CTestboard::GetRpcCallId(string &cmdName)
{
	return rpc_GetRpcCallId(cmdName);
}


void CTestboard::GetRpcTimestamp(stringR &t)
{
	t = rpc_timestamp;
}


int32_t CTestboard::GetRpcCallCount()
{
	return rpc_cmdListSize;
}


bool CTestboard::GetRpcCallName(int32_t id, stringR &callName)
{
	if (id < 0 || id >= rpc_cmdListSize)
	{
		callName.clear();
		return false;
	}
	callName = rpc_cmdlist[id].name;
	return true;
}


#define CLINE(text) text "\n"

void CTestboard::GetInfo(stringR &info)
{
	int fw = fw_version;
	int sw = sw_version;
	char s[256];
	snprintf(s,   255, "Board id:    %i\n", dtbConfig.board);	info = s;
	snprintf(s,   255, "HW version:  %s\n", dtbConfig.hw_version.c_str()); info += s;
	snprintf(s,   255, "FW version:  %i.%i\n", fw/256, fw & 0xff); info += s;
	snprintf(s,   255, "SW version:  %i.%i\n", sw/256, sw & 0xff); info += s;
	snprintf(s,   255, "Options:     %s\n", dtbConfig.hw_options.c_str()); info += s;
	snprintf(s,   255, "USB id:      %s\n", dtbConfig.usb_id.c_str()); info += s;
	snprintf(s,   255, "MAC address: %012llX\n", dtbConfig.mac_address); info += s;
	snprintf(s,   255, "Hostname:    %s\n", dtbConfig.hostname.c_str()); info += s;
	snprintf(s,   255, "Comment:     %s\n", dtbConfig.comment.c_str()); info += s;
}


uint16_t CTestboard::GetBoardId()
{
	return dtbConfig.board;
}


void CTestboard::GetHWVersion(stringR &version)
{
	version = dtbConfig.hw_version;
}


uint16_t CTestboard::GetFWVersion()
{
	return fw_version;
}


uint16_t CTestboard::GetSWVersion()
{
	return sw_version;
}




// === service ==============================================================

void CTestboard::Welcome()
{
	_SetLED(0x04);	mDelay(80);
	_SetLED(0x0c);	mDelay(80);
	_SetLED(0x09);	mDelay(80);
	_SetLED(0x03);	mDelay(80);
	_SetLED(0x02);	mDelay(80);
	_SetLED(0x00);	mDelay(80);

	_SetLED(0x01);	mDelay(80);
	_SetLED(0x03);	mDelay(80);
	_SetLED(0x06);	mDelay(80);
	_SetLED(0x0c);	mDelay(80);
	_SetLED(0x08);	mDelay(80);
	_SetLED(0x00);	mDelay(80);
}

void CTestboard::SetLed(uint8_t x)
{
	_SetLED(x);
}

// Turn on/off specific LED, negative is off, positive is on
void CTestboard::ToggleLed(uint8_t x, bool on) {
  if(on) { ledstatus |= x; }
  else { ledstatus &= ~x; }
  _SetLED(ledstatus);
}

const uint16_t CTestboard::flashUpgradeVersion = 0x0100;


uint16_t CTestboard::UpgradeGetVersion()
{
	return flashUpgradeVersion;
}


uint8_t CTestboard::UpgradeStart(uint16_t version)
{
	if (version != flashUpgradeVersion) THROW_UGR(ERR_VERSION, flash_error.GetErrorNr());
	flash_error.Reset();
	flashMem = new CFlashMemory();
	flashMem->Assign(2097152); // 2 MB (EPCS16)
	ugRecordCounter = 0;
	return flash_error.GetErrorNr();
}


uint8_t CTestboard::UpgradeData(string &record)
{
	if (!flashMem) THROW_UGR(ERR_MEMASSIGN, flash_error.GetErrorNr());
	CSRecordReader rec;
	rec.Translate(record.data(), *flashMem);
	if (IS_ERROR_UG) { delete flashMem; flashMem = 0; }
	ugRecordCounter++;
	return flash_error.GetErrorNr();
}


uint8_t CTestboard::UpgradeError()
{
	return flash_error.GetErrorNr();
}


void CTestboard::UpgradeErrorMsg(stringR &msg)
{
	msg = flash_error.GetErrorMsg();
}


void CTestboard::UpgradeExec(uint16_t recordCount)
{
	if (!flashMem) THROW_UG(ERR_MEMASSIGN);
	if (ugRecordCounter != recordCount) THROW_UG(ERR_RECCOUNT);
	SetLed(15);
	flashMem->WriteFlash();
	SetLed(0);
}


// === DTB initialization ===================================================

CTestboard::CTestboard()
{
	rpc_io = &usb; // USB default interface
	flashMem = 0;  // no memory assigned for upgrade

	daq_mem_base = 0;

	// stop all DMA channels
	DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_CONTROL, 0);


	Init();
}

void CTestboard::Init()
{
	// delete assigned memory for flash upgrade
	if (flashMem) { delete flashMem; flashMem = 0; }


	// --- shutdown DAQ ---------------------------------
	// close all open DAQ channels
	Daq_Close();



	// --- clear main control register
	mainCtrl = 0;
	_MainControl(0);

	isPowerOn = false;

	// --- signal settings
	sig_level_clk = 5;
	sig_level_ctr = 5;
	sig_level_sda = 5;
	sig_level_tin = 5;
	sig_offset    = 8;
	Sig_Off();
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x40); // tout ena
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));

	// --- signal probe
	SignalProbeD1(PROBE_OFF);
	SignalProbeD2(PROBE_OFF);

	Adv3224Init();

	// --- switch all LEDs off
	_SetLED(0x00);

	// --- setup I2C master controller
	I2C_Main_Init();
	I2C_External_Init();

	// --- default VD, VA settings
	vd = mV_to_DAC(2200);    // mV
	id = uA100_to_DAC(3000); // *100 uA
	va = mV_to_DAC(2000);    // mV
	ia = uA100_to_DAC(3000); // *100 uA

	InitDac();
	InitDacR4S0();
	InitDacR4S1();
	Poff();

	r4s_vcal  = 0;
	r4s_rgpr  = 0;
	r4s_rgsh  = 0;
	r4s_vref  = 0;
	r4s_vaux1 = 0;
	r4s_vaux2 = 0;
	r4s_vaux3 = 0;

	R4S_SetDAC(0, 0);
	R4S_SetDAC(1, 0);
	R4S_SetDAC(2, 0);
	R4S_SetDAC(3, 0);
	R4S_SetDAC(4, 0);
	R4S_SetDAC(5, 0);
	R4S_SetDAC(6, 0);
	R4S_SetDAC(7, 0);

	for (int i=0x100; i<0x200; i+=4) R4S_WRITE(i, 0);
}


// === timing ===============================================================

void CTestboard::cDelay(uint16_t clocks)
{
	uint16_t us = clocks / 40;
	if (us == 0) us = 1;
	uDelay(us);
}

void CTestboard::uDelay(uint16_t us)
{
	usleep(us);
}

void CTestboard::mDelay(uint16_t ms)
{
	uint16_t i;
	for (i=0; i<ms; i++) usleep(1000);
}


void CTestboard::SetClockSource(uint8_t source)
{
	if (source > 1) return;

	if (((source == 0) && (_MainStatus() & MAINSTATUS_CLK_ACTIVE_DAQ))
	 || ((source == 1) && !(_MainStatus() & MAINSTATUS_CLK_ACTIVE_DAQ)))
		mainCtrl |= MAINCTRL_CLK_SEL_DAQ;

	if (((source == 0) && (_MainStatus() & MAINSTATUS_CLK_ACTIVE_SMPL))
	 || ((source == 1) && !(_MainStatus() & MAINSTATUS_CLK_ACTIVE_SMPL)))
		mainCtrl |= MAINCTRL_CLK_SEL_SMPL;

	if (mainCtrl & (MAINCTRL_CLK_SEL_DAQ | MAINCTRL_CLK_SEL_SMPL))
	{
		_MainControl(mainCtrl);
		uDelay(1);
		mainCtrl &= ~(MAINCTRL_CLK_SEL_DAQ | MAINCTRL_CLK_SEL_SMPL);
		_MainControl(mainCtrl);
		uDelay(1);
		_MainControl(mainCtrl | MAINCTRL_PLL_RESET);
		uDelay(1);
		_MainControl(mainCtrl);
		uDelay(10);
	}
}


bool CTestboard::IsClockPresent()
{
	return (_MainStatus() & MAINSTATUS_CLK_EXT_BAD) == 0;
}




/*
	LVDS2LCDS Chip DACs:
	0  Bit 3..0: CLK amplitude
	1  Bit 3..0: CTR amplitude
	2  Bit 3..0: SDA amplitude
	3  Bit 3..0: TIN amplitude
	4  Bit    0: TIN LVDS select
	5  Bit 3..0: signal offset for CLK, CTR, SDA, TIN
*/
void CTestboard::Sig_SetLevel(uint8_t signal, uint8_t level)
{
	if (level > 15) level = 15;

	unsigned char dac;
	switch (signal)
	{
		case SIG_CLK: sig_level_clk = level; dac = 0x00; break;
		case SIG_CTR: sig_level_ctr = level; dac = 0x10; break;
		case SIG_SDA: sig_level_sda = level; dac = 0x20; break;
		case SIG_TIN: sig_level_tin = level; dac = 0x30; break;
		default: return;
	}
	if (isPowerOn)
	{
		IOWR_8DIRECT(LCDS_IO_BASE, 0, dac + level);
		while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	}
}

void CTestboard::Sig_SetOffset(uint8_t offset)
{
	if (offset > 15) offset = 15;
	sig_offset = offset;
	if (isPowerOn)
	{
		IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x50 + offset);
		while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	}
}


void CTestboard::Sig_Off()
{
	// configure lvds2lcds
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x00); // clk
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x10); // ctr
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x20); // sda
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x30); // tin
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x50); // offset
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
}

void CTestboard::Sig_Restore()
{
	// configure lvds2lcds
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x00 + sig_level_clk); // clk
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x10 + sig_level_ctr); // ctr
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x20 + sig_level_sda); // sda
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x30 + sig_level_tin); // tin
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x50 + sig_offset); // offset
	while(IORD_8DIRECT(LCDS_IO_BASE, 0));
}


void CTestboard::Sig_SetLVDS()
{
	mainCtrl &= ~MAINCTRL_TERM;
	_MainControl(mainCtrl);
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x41);
}

void CTestboard::Sig_SetLCDS()
{
	mainCtrl |=  MAINCTRL_TERM;
	_MainControl(mainCtrl);
	IOWR_8DIRECT(LCDS_IO_BASE, 0, 0x40);
}


// === VD/VA power supply control ===========================================

// DAC calibration
#define DAC_V0   625	// DAC value for V = 0
#define DAC_VS  5103  // DAC voltage scale factor

#define DAC_I0     0  // DAC value for I = 0
#define DAC_IS  1030  // DAC current scale factor

// ADC calibration
#define ADC_uV  1007  // ADC voltage step [uV]
#define ADC_uA   804  // ADC current step [uA]


void CTestboard::Pon()
{
	if (isPowerOn) return;

	SetDac(0, 184);	// va = 1V;
	SetDac(2, 184);	// vd = 1V;

	mainCtrl |= MAINCTRL_PWR_ON;
	_MainControl(mainCtrl);
	isPowerOn = true;

	SetDac(1, ia);
	SetDac(0, va);
	SetDac(3, id);
	SetDac(2, vd);

	r4s_Von();
	Sig_Restore();
}


void CTestboard::Poff()
{
	Sig_Off();
	r4s_Voff();

	SetDac(0, 184);	// va = 1V;
	SetDac(2, 184);	// vd = 1V;
	mainCtrl &= ~(MAINCTRL_PWR_ON | MAINCTRL_ADCENA);
	_MainControl(mainCtrl);
	isPowerOn = false;

	Daq_Close();
}


void CTestboard::_SetVD(uint16_t mV)
{
    vd = mV_to_DAC(mV);
    if (isPowerOn) SetDac(2,vd);
}


void CTestboard::_SetVA(uint16_t mV)
{
  	va = mV_to_DAC(mV);
	if (isPowerOn) SetDac(0,va);
}


uint16_t CTestboard::_GetVD()
{
	return ADC_to_mV(ReadADC(2));
}


uint16_t CTestboard::_GetVA()
{
	return ADC_to_mV(ReadADC(0));
}


void CTestboard::_SetID(uint16_t uA100)
{
    id = uA100_to_DAC(uA100);
    if (isPowerOn) SetDac(3,id);
}


void CTestboard::_SetIA(uint16_t uA100)
{
    ia = uA100_to_DAC(uA100);
    if (isPowerOn) SetDac(1,ia);
}


uint16_t CTestboard::_GetID()	// get VD current in 100 uA
{
  return ADC_to_uA100(ReadADC(3));
}


uint16_t CTestboard::_GetIA()	// get VA current in 100 uA
{
  return ADC_to_uA100(ReadADC(1));
}


uint16_t CTestboard::_GetVD_Reg()  // get VD reg at the capacitor in mV
{
	return ReadADC(4)/2;
}


uint16_t CTestboard::_GetVDAC_Reg()  // get VDAC reg at the capacitor in mV
{
	return ReadADC(5)/2;
}


uint16_t CTestboard::_GetVD_Cap()  // get unregulated VD at the capacitor in mV
{
	return ReadADC(6);
}


unsigned int CTestboard::mV_to_DAC(int mV)
{
	if (mV > 4000) mV = 4000; else if (mV < 0) mV = 0;
	long dac = (752817-229*(long)mV)/3333;
	if (dac>1023) dac = 1023; else if (dac<0) dac = 0;
	return (unsigned int)dac;
}


unsigned int CTestboard::uA100_to_DAC(int ua100)
{
	if (ua100 > 12000) ua100 = 12000; else if (ua100 < 0) ua100 = 0;
	unsigned long dac = (105*(unsigned long)ua100)/3300;
	if (dac>1023) dac = 1023; else if (dac<0) dac = 0;
	return (unsigned int)dac;
}


int CTestboard::ADC_to_mV(unsigned int dac)
{
	return (dac*ADC_uV)/1000;
}


int CTestboard::ADC_to_uA100(unsigned int dac)
{
	return (dac*ADC_uA)/100;
}


void CTestboard::InitDac()
{
	int res;
	// --- 0111100(0) 1 11110000 1 00111100

	// send slave address 0x3c
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x78);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

    // switch to extended command
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0xf0);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

    // all channels power up
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x3c);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);
}


void CTestboard::InitDacR4S0()
{
	int res;
	// --- 0111100(0) 1 11110000 1 00111100

	// send slave address 0x3c
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x78);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

    // switch to extended command
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0xf0);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

    // all channels power up
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x3c);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);
}


void CTestboard::InitDacR4S1()
{
	int res;
	// --- 0111101(0) 1 11110000 1 00111100

	// send slave address 0x3d
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x7A);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

    // switch to extended command
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0xf0);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

    // all channels power up
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x3c);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);
}


void CTestboard::SetDac(int addr, int value)
{
	int res;
	value &= 0x3ff;

	// send slave address 0x3c
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x78);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// load dac addr and data D9...D6
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, ((addr&3)<<4) | (value>>6));
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// send data D5...D0
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, value <<2);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);
}


void CTestboard::SetDacR4S0(int addr, int value)
{
	int res;
	value &= 0x3ff;

	// send slave address 0x3c
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x78);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

	// load dac addr and data D9...D6
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, ((addr&3)<<4) | (value>>6));
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

	// send data D5...D0
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, value <<2);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);
}


void CTestboard::SetDacR4S1(int addr, int value)
{
	int res;
	value &= 0x3ff;

	// send slave address 0x3d
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, 0x7A);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

	// load dac addr and data D9...D6
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, ((addr&3)<<4) | (value>>6));
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);

	// send data D5...D0
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, TXR, value <<2);
	IOWR_8DIRECT(I2C_EXTERNAL_BASE, CR,  WR|STO);
	while ((res = IORD_8DIRECT(I2C_EXTERNAL_BASE, SR)) & TIP);
}


unsigned int CTestboard::ReadADC(unsigned char addr)
{
	int res;
	// send slave address 0x35
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x6a);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// setup 11010010: internal reference, internal clock, unipolar
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0xd2);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// configuration 011aaaa1: single channel addr, single ended
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x61|((addr&0x0f)<<1));
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// send slave address 0x35 (read)
	IOWR_8DIRECT(I2C_MAIN_BASE, TXR, 0x6b);
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  STA|WR);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);

	// read D11...D8
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  RD);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);
	unsigned int value = IORD_8DIRECT(I2C_MAIN_BASE, RXR) & 0x0f;

	// read D7...D0
	IOWR_8DIRECT(I2C_MAIN_BASE, CR,  RD|ACK|STO);
	while ((res = IORD_8DIRECT(I2C_MAIN_BASE, SR)) & TIP);
	value = (value << 8) | IORD_8DIRECT(I2C_MAIN_BASE, RXR);

	return value;
}


void CTestboard::HVon()
{
	mainCtrl |= MAINCTRL_HV_ON;
	_MainControl(mainCtrl);
}


void CTestboard::HVoff()
{
	mainCtrl &= ~MAINCTRL_HV_ON;
	_MainControl(mainCtrl);
}


uint8_t CTestboard::GetStatus()
{
	return _MainStatus();
}


// === digital signal probe =================================================

void CTestboard::SignalProbeD1(uint8_t signal)
{
	if (signal <= 29) _Probe1(signal);
}


void CTestboard::SignalProbeD2(uint8_t signal)
{
	if (signal <= 29) _Probe2(signal);
}


// === analog signal probe =============================================

void CTestboard::SignalProbeA1(uint8_t signal)
{
	uint8_t sig = signal << 1;

	uint16_t delay = 200;
	while (IORD(CROSSPOINT_SWITCH_BASE, 0) & 1)
	{
		uDelay(1);
		if (--delay == 0) return;
	}

	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 4, sig);   // A1+
	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 5, sig+1); // A1-
	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, 0xFF);
}


void CTestboard::SignalProbeA2(uint8_t signal)
{
	uint8_t sig = signal << 1;

	uint16_t delay = 200;
	while (IORD(CROSSPOINT_SWITCH_BASE, 0) & 1)
	{
		uDelay(1);
		if (--delay == 0) return;
	}

	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 6, sig);   // A2+
	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 7, sig+1); // A2-
	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, 0xFF);
}


void CTestboard::SignalProbeADC(uint8_t signal, uint8_t gain)
{
	if (signal >= 8) return;
	if (gain > 3) gain = 3;
	uint8_t sig = signal << 1;

	uint16_t delay = 200;
	while (IORD(CROSSPOINT_SWITCH_BASE, 0) & 1)
	{
		uDelay(1);
		if (--delay == 0) return;
	}

	switch (gain)
	{
	case 0: // x1
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, 16);    // ADC+ (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 1, 16);    // ADC- (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 2, sig+1); // ADC+ (x1)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 3, sig);   // ADC- (x1)
		break;
	case 1: // x2
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, sig+1); // ADC+ (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 1, sig);   // ADC- (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 2, sig);   // ADC+ (x1)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 3, sig+1); // ADC- (x1)
		break;
	case 2: // x3
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, sig+1); // ADC+ (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 1, sig);   // ADC- (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 2, 16);    // ADC+ (x1)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 3, 16);    // ADC- (x1)
		break;
	case 3: // x4
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, sig+1); // ADC+ (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 1, sig);   // ADC- (x3)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 2, sig+1); // ADC+ (x1)
		IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 3, sig);   // ADC- (x1)
		break;
	}
	IOWR_8DIRECT(CROSSPOINT_SWITCH_BASE, 0, 0xFF);
}




// --- Data aquisition ------------------------------------------------------


uint32_t CTestboard::Daq_Open(uint32_t buffersize)
{
	// close last DAQ session if still active
	Daq_Close();

	// set size limits for memory allocation
	if (buffersize > 50000000) buffersize = 50000000;  // max 50 Mio samples
	if (buffersize < 8192) buffersize = 8192;  // min 8192 samples

	// allocate memory
	daq_mem_base = new uint16_t[buffersize];
	if (daq_mem_base == 0) return 0;
	daq_mem_size = buffersize;

	// set DMA to allocated memory
	DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_MEM_BASE, (unsigned long)(daq_mem_base));
	DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_MEM_SIZE, daq_mem_size);

	alt_dcache_flush(daq_mem_base, buffersize*2);

	return daq_mem_size;
}

void CTestboard::Daq_Close()
{
	if (daq_mem_base)
	{
		Daq_Stop();
		uDelay(1);
		delete[] daq_mem_base;
		daq_mem_base = 0;
	}
}

void CTestboard::Daq_Start()
{
	if (daq_mem_base)
	{
		// clear buffer and enable daq
		DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_CONTROL, 1);
	}
}


void CTestboard::Daq_Stop()
{
	if (daq_mem_base)
	{
		DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_CONTROL, 0);
	}
}

uint32_t CTestboard::Daq_GetSize()
{
	if (daq_mem_base == 0) return 0;

	// read dma status
	int32_t status = DAQ_READ(DAQ_DMA_0_BASE, DAQ_CONTROL);
	int32_t rp = DAQ_READ(DAQ_DMA_0_BASE, DAQ_MEM_READ);
	int32_t wp = DAQ_READ(DAQ_DMA_0_BASE, DAQ_MEM_WRITE);

	// correct write pointer overrun at memory overflow
	if (status & DAQ_MEM_OVFL) if (--wp < 0) wp += daq_mem_size;

	// calculate available words in memory (-> fifosize)
	int32_t diff = wp - rp;
	int32_t fifosize = diff;
	if (fifosize < 0) fifosize += daq_mem_size;

	return fifosize;
}

template <class T>
inline T* Uncache(T *x) { return (T*)(((unsigned long)x) | 0x80000000); }

uint8_t CTestboard::Daq_Read(vectorR<uint16_t> &data,
		 uint32_t blocksize)
{
	uint32_t availsize;
	return Daq_Read(data, blocksize, availsize);
}


uint8_t CTestboard::Daq_Read(vectorR<uint16_t> &data,
		uint32_t blocksize, uint32_t &availsize)
{
	data.clear();

	if (daq_mem_base == 0) { availsize = 0; return 0; }

	// limit maximal block size
	if (blocksize > 0x800000) blocksize = 0x800000;
	if (blocksize > daq_mem_size) blocksize = daq_mem_size;

	// read dma status
	int32_t status = DAQ_READ(DAQ_DMA_0_BASE, DAQ_CONTROL) ^ 1;
	int32_t rp = DAQ_READ(DAQ_DMA_0_BASE, DAQ_MEM_READ);
	int32_t wp = DAQ_READ(DAQ_DMA_0_BASE, DAQ_MEM_WRITE);

	// correct write pointer overrun at memory overflow
	if (status & DAQ_MEM_OVFL) if (--wp < 0) wp += daq_mem_size;

	// calculate available words in memory (-> fifosize)
	int32_t fifosize = wp - rp;
	if (fifosize < 0) fifosize += daq_mem_size;

	// calculate transfer block size (-> blocksize)
	if (int32_t(blocksize) > fifosize) blocksize = fifosize;

	// return remaining data size
	availsize = fifosize - blocksize;

	// allocate space in vector or return empty data
	if (blocksize > 0) data.reserve(blocksize);
	else return uint8_t(status);

	// --- send 1st part of the data block
	int32_t size1 = daq_mem_size - rp;
	if (size1 > int32_t(blocksize)) size1 = blocksize;

	// copy data to vector
	uint16_t *p = Uncache(daq_mem_base) + rp;
	data.insert(data.end(), p, p + size1);
	blocksize -= size1;

	// --- send 2ns part of the data block
	if (blocksize > 0)
	{
		p = Uncache(daq_mem_base);
		data.insert(data.end(), p, p + blocksize);
		rp = blocksize;
	}
	else rp += size1;

	// update read pointer
	DAQ_WRITE(DAQ_DMA_0_BASE, DAQ_MEM_READ, rp);

	return uint8_t(status);
}


uint8_t CTestboard::Daq_Read(HWvectorR<uint16_t> &data,
		uint32_t blocksize)
{
	uint32_t availsize;
	return Daq_Read(data, blocksize, availsize);
}


uint8_t CTestboard::Daq_Read(HWvectorR<uint16_t> &data,
		uint32_t blocksize, uint32_t &availsize)
{
	data.base = 0;
	data.s1 = 0;
	data.s2 = 0;

	if (daq_mem_base == 0) { availsize = 0; return 0; }

	// limit maximal block size
	if (blocksize > 0x800000) blocksize = 0x800000;
	if (blocksize > daq_mem_size) blocksize = daq_mem_size;

	// read dma status
	data.base = DAQ_DMA_0_BASE;
	int32_t status = DAQ_READ(data.base, DAQ_CONTROL) ^ 1;
	data.rp = DAQ_READ(data.base, DAQ_MEM_READ);
	int32_t wp = DAQ_READ(data.base, DAQ_MEM_WRITE);

	// correct write pointer overrun at memory overflow
	if (status & DAQ_MEM_OVFL) if (--wp < 0) wp += daq_mem_size;

	// calculate available words in memory (-> fifosize)
	int32_t fifosize = wp - data.rp;
	if (fifosize < 0) fifosize += daq_mem_size;

	// calculate transfer block size (-> blocksize)
	if (int32_t(blocksize) > fifosize) blocksize = fifosize;

	// return remaining data size
	availsize = fifosize - blocksize;

	// allocate space in vector or return empty data
	if (blocksize == 0) return uint8_t(status);

	// --- send 1st part of the data block
	int32_t size1 = daq_mem_size - data.rp;
	if (size1 > int32_t(blocksize)) size1 = blocksize;

	// data block 1
	data.p1 = daq_mem_base + data.rp;
	data.s1 = size1;
	blocksize -= size1;

	// --- data block 2
	if (blocksize > 0)
	{
		data.p2 = daq_mem_base;
		data.s2 = blocksize;
		data.rp = blocksize;
	}
	else data.rp += size1;

	return uint8_t(status);
}



// === ROC4Sens adapter test functions ======================================


void CTestboard::r4s_SetHoldPos(uint8_t t)
{
	R4S_WRITE(R4S_HOLD_POS, t);
}


void CTestboard::r4s_AdcDelay(uint8_t t)
{
	R4S_WRITE(R4S_ADC_DELAY, t);
}


void CTestboard::r4s_SetSequence(vector<uint32_t> &prog)
{
	unsigned int n = (unsigned int)prog.size();
	if (n > 256) n = 256;

	unsigned int p = R4S_SEQ_BASE;
	for (unsigned int i=0; i<n; i++)
	{
		R4S_WRITE(p, prog[i]);
		p += 4;
	}
	if (n < 256) R4S_WRITE(p, 0);
}


void CTestboard::r4s_SetRegX(vector<uint32_t> &shr_x)
{
	if (shr_x.size() < 5) return;
	R4S_WRITE(R4S_SRX_BASE,     shr_x[0]);
	R4S_WRITE(R4S_SRX_BASE + 4, shr_x[1]);
	R4S_WRITE(R4S_SRX_BASE + 8, shr_x[2]);
	R4S_WRITE(R4S_SRX_BASE +12, shr_x[3]);
	R4S_WRITE(R4S_SRX_BASE +16, shr_x[4]);
}


void CTestboard::r4s_SetRegY(vector<uint32_t> &shr_y)
{
	if (shr_y.size() < 5) return;
	R4S_WRITE(R4S_SRY_BASE,     shr_y[0]);
	R4S_WRITE(R4S_SRY_BASE + 4, shr_y[1]);
	R4S_WRITE(R4S_SRY_BASE + 8, shr_y[2]);
	R4S_WRITE(R4S_SRY_BASE +12, shr_y[3]);
	R4S_WRITE(R4S_SRY_BASE +16, shr_y[4]);
}


/*
void CTestboard::r4s_SetPixCal(uint8_t x, uint8_t y)
{
	if (x < 155 && y < 160)
	{
		R4S_WRITE(R4S_CALX, x);
		R4S_WRITE(R4S_CALY, y);
	}
}
*/



void CTestboard::r4s_Enable(uint8_t t)
{
	R4S_WRITE(R4S_DAQ, t);
}


void CTestboard::r4s_Start()
{
	// if still running then stop
	if (R4S_READ(0) & 1) R4S_WRITE(R4S_CTRL, 2);

	// start
	R4S_WRITE(R4S_CTRL, 1);
}

void CTestboard::r4s_Stop()
{
	R4S_WRITE(R4S_CTRL, 2);
}


bool CTestboard::r4s_Running()
{
	return R4S_READ(R4S_CTRL) & 1;
}



#define DAC_CHANNEL_VCAL   0
#define DAC_CHANNEL_RGSH   1
#define DAC_CHANNEL_RGPR   2
#define DAC_CHANNEL_VREF   3
#define DAC_CHANNEL_VAUX1  7
#define DAC_CHANNEL_VAUX2  8
#define DAC_CHANNEL_VAUX3  9


void CTestboard::r4s_SetVcal(uint16_t mV)
{
	r4s_vcal = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_VCAL, r4s_vcal);
}


void CTestboard::r4s_SetRgsh(uint16_t mV)
{
	r4s_rgsh = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_RGSH, r4s_rgsh);
}


void CTestboard::r4s_SetRgpr(uint16_t mV)
{
	r4s_rgpr = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_RGPR, r4s_rgpr);
}


void CTestboard::r4s_SetVref(uint16_t mV)
{
	r4s_vref = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_VREF, r4s_vref);
}


void CTestboard::r4s_SetVaux1(uint16_t mV)
{
	r4s_vaux1 = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_VAUX1, r4s_vaux1);
}


void CTestboard::r4s_SetVaux2(uint16_t mV)
{
	r4s_vaux2 = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_VAUX2, r4s_vaux2);
}


void CTestboard::r4s_SetVaux3(uint16_t mV)
{
	r4s_vaux3 = mV2DAC(mV);
	if (isPowerOn) R4S_SetDAC(DAC_CHANNEL_VAUX3, r4s_vaux3);
}


void CTestboard::r4s_Voff()
{
	R4S_SetDAC(DAC_CHANNEL_VCAL,  0);
	R4S_SetDAC(DAC_CHANNEL_RGSH,  0);
	R4S_SetDAC(DAC_CHANNEL_RGPR,  0);
	R4S_SetDAC(DAC_CHANNEL_VREF,  0);
	R4S_SetDAC(DAC_CHANNEL_VAUX1, 0);
	R4S_SetDAC(DAC_CHANNEL_VAUX2, 0);
	R4S_SetDAC(DAC_CHANNEL_VAUX3, 0);
}


void CTestboard::r4s_Von()
{
	R4S_SetDAC(DAC_CHANNEL_VCAL,  r4s_vcal);
	R4S_SetDAC(DAC_CHANNEL_RGSH,  r4s_rgsh);
	R4S_SetDAC(DAC_CHANNEL_RGPR,  r4s_rgpr);
	R4S_SetDAC(DAC_CHANNEL_VREF,  r4s_vref);
	R4S_SetDAC(DAC_CHANNEL_VAUX1, r4s_vaux1);
	R4S_SetDAC(DAC_CHANNEL_VAUX2, r4s_vaux2);
	R4S_SetDAC(DAC_CHANNEL_VAUX3, r4s_vaux3);
}


uint16_t CTestboard::mV2DAC(uint16_t mV)
{
	uint16_t n = (uint16_t)((mV*1023l)/2500l);
	if (n > 1023) n = 1023;
	return n;
}


void CTestboard::R4S_SetDAC(uint8_t channel, uint16_t value)
{
	if (channel < 4) SetDacR4S0(channel, value);
	else if (channel < 8) SetDacR4S1(channel-4, value);
}


void CTestboard::R4S_SetReg(uint8_t reg, uint32_t value)
{
	R4S_WRITE(reg, value);
}

