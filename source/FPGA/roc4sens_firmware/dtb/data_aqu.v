// data_aqu.v


module data_aqu
(
	input reset,
	input clk,
	input serclk,

	// control interface
	input  [8:0]ctrl_address,
	input  ctrl_write,
	input  [31:0]ctrl_writedata,
	input  ctrl_read,
	output [31:0]ctrl_readdata,
	
	// DAQ interface
	input daq_running,
	output daq_write,
	output [15:0]daq_writedata,

	input  RBO,
	output RBI,
	
	output PHI1,
	output PHI2,
	output SELCLK,
	
	output LA,
	output HOLD,
	output VCAL_ENA,
	output VCAL_PULSE,
	
	output ADC_clk,
	output ADC_enable,
	
	input [11:0]ADC_data,
	input ADC_or,
	
	output [11:0]probe
);

	// --- ROC4Sens clock ---------------------------------------------
	reg [4:0]clock;
	always @(posedge clk or posedge reset)
	begin
		if (reset) clock <= 5'd0;
		else clock <= clock + 5'd1;
	end
	
	// --- ADC --------------------------------------------------------
	
	roc4sens_daq daq
	(
		.clk(clk),
		.serclk(serclk),
		.res(reset),
		.ctrl_address(ctrl_address),
		.ctrl_write(ctrl_write),
		.ctrl_writedata(ctrl_writedata),
		.ctrl_read(ctrl_read),
		.ctrl_readdata(ctrl_readdata),
		.phi1(PHI1),
		.phi2(PHI2),
		.sclk(SELCLK),
		.rbi(RBI),
		.la(LA),
		.cal_ena(VCAL_ENA),
		.cal_pulse(VCAL_PULSE),
		.hold(HOLD),
		.adc_clk(ADC_clk),
		.adc_ena(ADC_enable),
		.adc_data(ADC_data),
		.adc_or(ADC_or),
		.daq_write(daq_write),
		.daq_writedata(daq_writedata),
		.probe(probe)
	);
	
endmodule
