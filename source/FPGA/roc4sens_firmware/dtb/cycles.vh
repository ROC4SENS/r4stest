// cycles.vh

localparam CYC_WAIT     = 4'b0000;
localparam CYC_RES0     = 4'b0010;
localparam CYC_RES1     = 4'b0011;
localparam CYC_DIRX     = 4'b0100;
localparam CYC_DIRY     = 4'b0110;
localparam CYC_SHIFT0   = 4'b1000;
localparam CYC_SHIFT1   = 4'b1001;
localparam CYC_DSHIFT00 = 4'b1100;
localparam CYC_DSHIFT01 = 4'b1101;
localparam CYC_DSHIFT10 = 4'b1110;
localparam CYC_DSHIFT11 = 4'b1111;
