// output_reg.v


module output_reg
(
	input clk,
	input reset,
	input D,
	output reg Q
);
	always @(posedge clk or posedge reset)
	begin
		if (reset) Q <= 0;
		else Q <= D;
	end

endmodule
