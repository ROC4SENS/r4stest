// === roc4sens_daq.v =======================================================
//
//    ROC4sens controller and DAQ for DTB
//    Beat Meier PSI
//    7/24/2017
//
// ==========================================================================


module roc4sens_daq
(
// clock reset
	input clk,     // daq clock
	input serclk,  // cycle clock (f_serclk = 4*clk)
	input res,     // async reset

	// cpu control interface
	input  [8:0]ctrl_address,
	input  ctrl_write,
	input  [31:0]ctrl_writedata,
	input  ctrl_read,
	output [31:0]ctrl_readdata,

	// roc4sens signals	
	output phi1,
	output phi2,
	output sclk,
	output rbi,
	output la,
	output cal_ena,
	output cal_pulse,
	output hold,
	
	output adc_clk,       // ADC clock
	output adc_ena,       // ADC enable
	input [11:0]adc_data, // ADC 12 bit data
	input adc_or,         // ADC overrange
	
	// DAQ data
	output daq_write,
	output [15:0]daq_writedata,
	
	// probe signals
	output [11:0]probe
);


// === control interface ====================================================

/* --- registers ------------------------------
   0: [1] stop; [0] start;
   1: [1] slow_readout; [0] daq_enable
   2: [7:0] holdpos
   3: [4:0] adc_delay
   4: [31:0] srx[31:0]
   5: [31:0] srx[63:32]
   6: [31:0] srx[95:64]
   7: [31:0] srx[127:96]
   8: [31:0] srx[159:128]
   9: [31:0] sry[31:0]
  10: [31:0] sry[63:32]
  11: [31:0] sry[95:64]
  12: [31:0] sry[127:96]
  13: [26:0] sry[154:128]
*/

	// --- control/status bits
	reg start;          // sequencer start
	reg stop;           // sequencer abort if running
	reg slow_readout;   // 1=slow, 0=fast readout mode
	reg daq_enable;     // data will be sent to DAQ
	reg [154:0]srx;     // roc4sens x-register data
	reg [159:0]sry;     // roc4sens y-register data
	reg [7:0]holdpos;   // contol hold signal position in time
	reg [4:0]adc_delay; // ADC sampling point
	wire busy;          // 1=sequencer running

	// --- write registers
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			slow_readout <= 1'd0;
			daq_enable   <= 1'd0;
			srx          <= 155'd0;
			sry          <= 160'd0;
			holdpos      <= 8'd0;
			adc_delay    <= 5'd0;
		end
		else if (ctrl_write)
		begin
			case (ctrl_address)
				 9'd1: {slow_readout, daq_enable } <= ctrl_writedata[1:0];
				 9'd2: holdpos      <= ctrl_writedata[7:0];
				 9'd3: adc_delay    <= ctrl_writedata[4:0];
				 9'd4: srx[31:0]    <= ctrl_writedata;
				 9'd5: srx[63:32]   <= ctrl_writedata;
				 9'd6: srx[95:64]   <= ctrl_writedata;
				 9'd7: srx[127:96]  <= ctrl_writedata;
				 9'd8: srx[154:128] <= ctrl_writedata[26:0];
				 9'd9: sry[31:0]    <= ctrl_writedata;
				9'd10: sry[63:32]   <= ctrl_writedata;
				9'd11: sry[95:64]   <= ctrl_writedata;
				9'd12: sry[127:96]  <= ctrl_writedata;
				9'd13: sry[159:128] <= ctrl_writedata;
			endcase
		end
	end

	// --- write pulse registers
	wire runctrl = ctrl_write && (ctrl_address == 9'd0);
	wire set_start = runctrl && ctrl_writedata[0];
	wire set_stop  = runctrl && ctrl_writedata[1];

	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			start <= 1'd0;
			stop  <= 1'd0;
		end
		else
		begin
			start <= set_start;
			stop  <= set_stop;
		end
	end
	
	// --- read busy flag on all adresses
	assign ctrl_readdata = {31'd0, busy};
	


	// === sequencer ==========================================================

	// --- roc4sens signals (DAQ clk domain) sent to cycle generator
	wire _la;
	wire _cal_ena;
	wire [2:0]_cal_pulse;
	wire [2:0]_hold;
	
	wire [3:0]cg_cmd; // cycle type
	wire davail;      // cycle with ADC data
	wire [6:0]seq_probe;

	seq_main sequence2
	(
		.clk(clk),
		.res(res),
		.start(start),
		.stop(stop),
		.running(busy),
		.cyc(cg_cmd),
		.data_x(srx),
		.data_y(sry),
		.holdpos(holdpos),
		.slow_readout(slow_readout),
		.write(ctrl_write && ctrl_address[8]),
		.writeaddress(ctrl_address[7:0]),
		.writedata(ctrl_writedata),
		.la(_la),
		.cal_ena(_cal_ena),
		.cal_pulse(_cal_pulse),
		.hold(_hold),
		.davail(davail),
		.probe(seq_probe)
	);


	// --- cycle generator (clk -> serclk clock crossing) ---------------------
	
	wire [1:0]cg_cycle; // cycle number for ADC

	cycle_gen cg
	(
		.clk(clk),
		.serclk(serclk),
		.res(res),
		.cmd(cg_cmd),
		.cycle(cg_cycle),
		.phi1(phi1),
		.phi2(phi2),
		.sclk(sclk),
		.rbi(rbi),
	
		.la_in(_la),
		.cal_ena_in(_cal_ena),
		.cal_pulse_in(_cal_pulse),
		.hold_in(_hold),
	
		.la(la),
		.cal_ena(cal_ena),
		.cal_pulse(cal_pulse),
		.hold(hold)
	);


	// --- ADC clock shift, data ------------------------------------------------
	wire [12:0]data;

	wire data_valid;         // data for DAQ available
	wire [12:0]adc_data_out; // combined ADC overrange and ADC data
	
	adc_data adc
	(
		.clk(clk),
		.serclk(serclk),
		.res(res),
		.cycle(cg_cycle),
		.davail(davail),
		.delay(adc_delay),
		.adc_clk(adc_clk),
		.data_in({adc_or, adc_data}),
		.valid(data_valid),
		.data_out(adc_data_out)
	);

	// --- DAQ data start marker
	reg mark_start;
	reg busydel;
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			mark_start <= 1'b0;
			busydel    <= 1'b0;
		end
		else
		begin
			busydel <= busy;
			if (busy && !busydel)   mark_start <= 1'b1;
			else if (daq_writedata) mark_start <= 1'b0;
		end
	end


	assign adc_ena = daq_enable;

	assign daq_write     = data_valid & daq_enable;
	assign daq_writedata = {mark_start, 2'd0, adc_data_out};


	// --- probe signals --------------------------------------------------------
	
	/* probe signals
		probe[11]  ADC data available
		probe[10]  HOLD
		probe[ 9]  CAL_PULSE
		probe[ 8]  CAL_ENA
		probe[ 7]  LA
		probe[ 6]  sequencer readout line
		probe[ 5]  readout sequence start (load first line)
		probe[ 4]  measure sequence start
		probe[ 3]  sequence end signal (token return)
		probe[ 2]  sequence start command (token start)
		probe[ 1]  sequnece aborted by stop command
		probe[ 0]  sequencer running
	*/
	
	assign probe = { davail, _hold[0], _cal_pulse[0], _cal_ena, _la,  seq_probe };

endmodule
