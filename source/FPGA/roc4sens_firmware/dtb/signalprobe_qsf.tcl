# Copyright (C) 1991-2014 Altera Corporation
# Your use of Altera Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Altera Program License 
# Subscription Agreement, Altera MegaCore Function License 
# Agreement, or other applicable license agreement, including, 
# without limitation, that your use is for the sole purpose of 
# programming logic devices manufactured by Altera and sold by 
# Altera or its authorized distributors.  Please refer to the 
# applicable agreement for further details.


# Quartus II 64-Bit Version 13.1.4 Build 182 03/12/2014 SJ Full Version
# File: signalprobe_qsf.tcl
# Generated on: Wed Dec 23 07:23:05 2015

# Note: This file contains a Tcl script generated from the SignalProbe Gui.
#       You can use this script to restore SignalProbes after deleting the DB
#       folder; at the command line use "quartus_cdb -t signalprobe_qsf.tcl".

package require ::quartus::chip_planner
package require ::quartus::project
project_open dtb -revision dtb
read_netlist
set had_failure 0

############
# Index: 1 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[0\]" -loc PIN_E16 -pin_name "demuxA\[0\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (demuxA\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[0\]\" -loc PIN_E16 -pin_name \"demuxA\[0\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (demuxA\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[0\]\" -loc PIN_E16 -pin_name \"demuxA\[0\]\" -io_std \"2.5 V\""
} 

############
# Index: 2 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[1\]" -loc PIN_B16 -pin_name "demuxA\[1\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (demuxA\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[1\]\" -loc PIN_B16 -pin_name \"demuxA\[1\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (demuxA\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|demux_ab:separator|doutA\[1\]\" -loc PIN_B16 -pin_name \"demuxA\[1\]\" -io_std \"2.5 V\""
} 

############
# Index: 3 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[0\]" -loc PIN_A16 -pin_name "data\[0\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (data\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[0\]\" -loc PIN_A16 -pin_name \"data\[0\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (data\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[0\]\" -loc PIN_A16 -pin_name \"data\[0\]\" -io_std \"2.5 V\""
} 

############
# Index: 4 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[1\]" -loc PIN_E15 -pin_name "data\[1\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (data\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[1\]\" -loc PIN_E15 -pin_name \"data\[1\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (data\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[1\]\" -loc PIN_E15 -pin_name \"data\[1\]\" -io_std \"2.5 V\""
} 

############
# Index: 5 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[2\]" -loc PIN_B15 -pin_name "data\[2\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (data\[2\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[2\]\" -loc PIN_B15 -pin_name \"data\[2\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (data\[2\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[2\]\" -loc PIN_B15 -pin_name \"data\[2\]\" -io_std \"2.5 V\""
} 

############
# Index: 6 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[3\]" -loc PIN_A15 -pin_name "data\[3\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (data\[3\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[3\]\" -loc PIN_A15 -pin_name \"data\[3\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (data\[3\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|dout\[3\]\" -loc PIN_A15 -pin_name \"data\[3\]\" -io_std \"2.5 V\""
} 

############
# Index: 7 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|error" -loc PIN_E14 -pin_name "frame_error" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (frame_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|error\" -loc PIN_E14 -pin_name \"frame_error\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (frame_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|error\" -loc PIN_E14 -pin_name \"frame_error\" -io_std \"2.5 V\""
} 

############
# Index: 8 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|nrzi_4b5b_decoder:dec|error" -loc PIN_B14 -pin_name "symbol_error" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (symbol_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|nrzi_4b5b_decoder:dec|error\" -loc PIN_B14 -pin_name \"symbol_error\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (symbol_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|nrzi_4b5b_decoder:dec|error\" -loc PIN_B14 -pin_name \"symbol_error\" -io_std \"2.5 V\""
} 

############
# Index: 9 #
############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[1\]" -loc PIN_C13 -pin_name "idle_error" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (idle_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[1\]\" -loc PIN_C13 -pin_name \"idle_error\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (idle_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[1\]\" -loc PIN_C13 -pin_name \"idle_error\" -io_std \"2.5 V\""
} 

#############
# Index: 10 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[0\]" -loc PIN_B13 -pin_name "hdr_error" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (hdr_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[0\]\" -loc PIN_B13 -pin_name \"hdr_error\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (hdr_error): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|module_decoder:decoderA|sme\[0\]\" -loc PIN_B13 -pin_name \"hdr_error\" -io_std \"2.5 V\""
} 

#############
# Index: 11 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[0\]" -loc PIN_A13 -pin_name "sym\[0\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (sym\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[0\]\" -loc PIN_A13 -pin_name \"sym\[0\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (sym\[0\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[0\]\" -loc PIN_A13 -pin_name \"sym\[0\]\" -io_std \"2.5 V\""
} 

#############
# Index: 12 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[1\]" -loc PIN_E12 -pin_name "sym\[1\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (sym\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[1\]\" -loc PIN_E12 -pin_name \"sym\[1\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (sym\[1\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[1\]\" -loc PIN_E12 -pin_name \"sym\[1\]\" -io_std \"2.5 V\""
} 

#############
# Index: 13 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[2\]" -loc PIN_D13 -pin_name "sym\[2\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (sym\[2\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[2\]\" -loc PIN_D13 -pin_name \"sym\[2\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (sym\[2\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[2\]\" -loc PIN_D13 -pin_name \"sym\[2\]\" -io_std \"2.5 V\""
} 

#############
# Index: 14 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[3\]" -loc PIN_F11 -pin_name "sym\[3\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (sym\[3\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[3\]\" -loc PIN_F11 -pin_name \"sym\[3\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (sym\[3\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[3\]\" -loc PIN_F11 -pin_name \"sym\[3\]\" -io_std \"2.5 V\""
} 

#############
# Index: 15 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[4\]" -loc PIN_E11 -pin_name "sym\[4\]" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (sym\[4\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[4\]\" -loc PIN_E11 -pin_name \"sym\[4\]\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (sym\[4\]): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|frame_detector:frame|pdata\[4\]\" -loc PIN_E11 -pin_name \"sym\[4\]\" -io_std \"2.5 V\""
} 

#############
# Index: 16 #
#############
set result [ make_sp  -src_name "deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|davail" -loc PIN_A14 -pin_name "davail" -io_std "2.5 V" ] 
if { $result == 0 } { 
	 puts "FAIL (davail): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|davail\" -loc PIN_A14 -pin_name \"davail\" -io_std \"2.5 V\""
} else { 
 	 puts "SET  (davail): make_sp  -src_name \"deser_tbm:inst1|deser400_PSI:ds400_I|header_realigner:realignerA|davail\" -loc PIN_A14 -pin_name \"davail\" -io_std \"2.5 V\""
} 

