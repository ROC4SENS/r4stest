// === seq_main.v ===========================================================
//
//    Programable sequener to control ROC4sens
//    Beat Meier PSI
//    7/24/2017
//
// ==========================================================================


module seq_main
(
	input clk,
	input res,

	input start,
	input stop,
	output running,

	output [3:0]cyc,

	// parameter
	input [154:0]data_x,
	input [159:0]data_y,
	input [7:0]holdpos,
	
	input slow_readout,
	
	input write,
	input [7:0]writeaddress,
	input [31:0]writedata,
	
	// outputs
	output la,
	output cal_ena,
	output [2:0]cal_pulse,
	output [2:0]hold,
	
	output davail,
	
	output [6:0]probe
);

	// --- command fetch ------------------------------------------------------
	wire cmd_start;
	wire cmd_next;
	
	reg [10:0]pc;
	wire [3:0]cmd;
	
	seq_ram ram
	(
		.clock(clk),
		.wren(write),
		.wraddress(writeaddress),
		.data(writedata),
		.rdaddress(pc),
		.q(cmd)
	);
	
	always @(posedge clk or posedge res)
	begin
		if (res) pc <= 11'd0;
		else
		begin
			if (cmd_start || stop) pc <= 11'd0;
			else if (cmd_next) pc <= pc + 11'd1;
		end
	end
	
	
	// --- command decoding ---------------------------------------------------
	reg  [15:0]tstart;
	wire [15:0]tend;
	
	wire tend_sum = |tend;
	
	always @(*)
	begin
		case (cmd)
			4'd0:  tstart = { 15'd0, tend_sum};
			4'd1:  tstart = { 14'd0, tend_sum,  1'd0 };
			4'd2:  tstart = { 13'd0, tend_sum,  2'b0 };
			4'd3:  tstart = { 12'd0, tend_sum,  3'b0 };
			4'd4:  tstart = { 11'd0, tend_sum,  4'b0 };
			4'd5:  tstart = { 10'd0, tend_sum,  5'b0 };
			4'd6:  tstart = {  9'd0, tend_sum,  6'b0 };
			4'd7:  tstart = {  8'd0, tend_sum,  7'b0 };
			4'd8:  tstart = {  7'd0, tend_sum,  8'b0 };
			4'd9:  tstart = {  6'd0, tend_sum,  9'b0 };
			4'd10: tstart = {  5'd0, tend_sum, 10'b0 };
			4'd11: tstart = {  4'd0, tend_sum, 11'b0 };
			4'd12: tstart = {  3'd0, tend_sum, 12'b0 };
			4'd13: tstart = {  2'd0, tend_sum, 13'b0 };
			4'd14: tstart = {  1'd0, tend_sum, 14'b0 };
			4'd15: tstart = {        tend_sum, 15'b0 };
		endcase
	end
	
	assign cmd_next = tend_sum;


	// === commands ===========================================================
/*
   0: stop
   1: seq_resetx
   2: seq_resety
   3: seq_loadx
   4: seq_loady
   5: seq_measure
   6: seq_readline
   7: seq_firstline
   8: seq_nextline
   9: nop
  10: nop
  11: nop
  12: nop
  13: nop
  14: nop
  15: nop
*/

	// --- token 0 ------------------------------------------------------------
	// start/stop
	// inputs: start
	// outputs: running
	
	seq_start_stop cmd0
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[0]),
		.tout(tend[0]),
		.start(start),
		.jump0(cmd_start),
		.running(running)
	);


	// --- token 1 ------------------------------------------------------------
	// seq_resetx: clear x shift register
	
	wire [3:0]cyc1;
	
	seq_resetx cmd1
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[1]),
		.tout(tend[1]),
		.cyc(cyc1)
	);


	// --- token 2 ------------------------------------------------------------
	// seq_resety: clear y shift register
	
	wire [3:0]cyc2;

	seq_resety cmd2
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[2]),
		.tout(tend[2]),
		.cyc(cyc2)
	);


	// --- token 3, token 4 ---------------------------------------------------
	// seq_loadxy: load x or y shift register with data
	// inputs: data_x, data_y
	
	wire [3:0]cyc3;

	seq_loadxy cmd34
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin_x(tstart[3]),
		.tin_y(tstart[4]),
		.tout(tend[3]),
		.cyc(cyc3),
		.data_x(data_x),
		.data_y(data_y)
	);

	assign tend[4] = tend[3];


	// --- token 5 ------------------------------------------------------------
	// seq_measure: sends a calibrate and a hold
	// inputs: holdpos
	
	seq_measure cmd5
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[5]),
		.tout(tend[5]),
		.la(la5),
		.cal_ena(cal_ena),
		.cal_pulse(cal_pulse),
		.hold(hold),
		.holdpos(holdpos)
	);


	// --- token 6 ------------------------------------------------------------
	// seq_readline: readout a line
	// inputs: slow_readout
	// outputs: davail

	wire [3:0]cyc6;
	
	seq_readline cmd6
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[6]),
		.tout(tend[6]),
		.cyc(cyc6),
		.la(la6),
		.davail(davail),
		.slow_readout(slow_readout)
	);


	// --- token 7 ------------------------------------------------------------
	// seq_firstline: move to first line

	wire [3:0]cyc7;

	seq_firstline cmd7
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[7]),
		.tout(tend[7]),
		.cyc(cyc7)
	);


	// --- token 8 ------------------------------------------------------------
	// seq_nextline: move to next line

	wire [3:0]cyc8;

	seq_nextline cmd8
	(
		.clk(clk),
		.res(res),
		.stop(stop),
		.tin(tstart[8]),
		.tout(tend[8]),
		.cyc(cyc8)
	);

	// --- token 9 ------------------------------------------------------------
	// not implemented
	seq_nop cmd9(clk, res, stop, tstart[9], tend[9]);
	
	// --- token 10 -----------------------------------------------------------
	// not implemented
	seq_nop cmd10(clk, res, stop, tstart[10], tend[10]);

	// --- token 11 -----------------------------------------------------------
	// not implemented
	seq_nop cmd11(clk, res, stop, tstart[11], tend[11]);
	
	// --- token 12 -----------------------------------------------------------
	// not implemented
	seq_nop cmd12(clk, res, stop, tstart[12], tend[12]);

	// --- token 13 -----------------------------------------------------------
	// not implemented
	seq_nop cmd13(clk, res, stop, tstart[13], tend[13]);
	
	// --- token 14 -----------------------------------------------------------
	// not implemented
	seq_nop cmd14(clk, res, stop, tstart[14], tend[14]);
	
	// --- token 15 -----------------------------------------------------------
	// not implemented
	seq_nop cmd15(clk, res, stop, tstart[15], tend[15]);


	assign cyc = cyc1 | cyc2 | cyc3 | cyc6 | cyc7 | cyc8;
	assign la = la5 | la6;
	
	
	assign probe =
	{
		tstart[6],       // read line
		tstart[7],       // readout start
		tstart[5],       // measure start
		tstart[0],       // sequence end signal
		tend[0],         // sequence start signal
		stop && running, // sequnece abort
		running          // running
	};
	
endmodule
