// === seq_readline.v =======================================================
//
//    sequencer command
//    readout the selected line (with low or high speed option)
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_readline
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output tout,
	
	output [3:0]cyc,
	output la,
	output reg davail,
	
	input slow_readout
);

	localparam LINELENGTH = 8'd157;
	`include "cycles.vh"

	// --- delay counter ------------------------------------------------------
	
	reg running;
		
	reg [2:0]state;
	reg [7:0]repcnt;
	wire next;
	reg [3:0]_cyc;

	assign la = running;

	wire cntena;
	delay_counter #(.WIDTH(8)) del(clk, res, repcnt, cntena, next);
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			_cyc    <= CYC_WAIT;
			state  <= 3'd0;
		end
		else if (stop)
		begin
			_cyc    <= CYC_WAIT;
			state  <= 3'd0;
		end
		else
		begin
			case (state)
				3'd0: if (tin)
					begin
						_cyc <= CYC_DIRX;
						state <= 3'd1;
					end
				3'd1:
					begin
						_cyc <= CYC_WAIT;
						state <= 3'd2;
					end
				3'd2:
					begin // shift to first pixel and read out
						_cyc <= CYC_SHIFT1;
						state <= slow_readout ? 3'd4 : 3'd3;
					end
				3'd3:
					begin // shift to next pixel and read out
						_cyc <= CYC_SHIFT0;
						if (next) // last pixel ?
						begin
							state <= 3'd7;
						end
					end
				3'd4:
					begin
						_cyc <= CYC_WAIT;
						state <= 3'd5;
					end
				3'd5:
					begin
						_cyc <= CYC_SHIFT0;
						state <= 3'd6;
					end
				3'd6:
					begin
						_cyc <= CYC_WAIT;
						state <= next ? 3'd7 : 3'd5;
					end
				3'd7:
					begin // shift bit out
						_cyc <= CYC_SHIFT0;
						state <= 3'd0;
					end
			endcase
		end // else
	end // always

	assign cntena = !(state == 3'd5);

	always @(*)
	begin
		case (state)
			3'd2: repcnt <= LINELENGTH;
			default: repcnt <= 8'd0;
		endcase
	end

	assign tout = (state == 3'd7);
	assign cyc  = running ? _cyc : 4'b0000;

	always @(posedge clk or posedge res)
	begin
		if (res) running <= 1'b0;
		else if (stop) running <= 1'b0;
		else if (tin)  running <= 1'b1;
		else if (tout) running <= 1'b0;
	end
	

	always @(posedge clk or posedge res)
	begin
		if (res) davail <= 1'b0;
		else if (stop) davail <= 1'b0;
		else davail <=  (state == 3'd3) || (state == 3'd4) || (state == 3'd6);
	end

endmodule
