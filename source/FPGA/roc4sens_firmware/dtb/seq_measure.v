// === seq_measure.v ========================================================
//
//    sequencer command
//    generate calibrate and hold signal
//    Beat Meier PSI
//    7/21/2017
//
// ==========================================================================


module seq_measure
(
	input clk,
	input res,
	input stop,
	
	input tin,
	output tout,

	output la,
	output cal_ena,
	output reg [2:0]cal_pulse,
	output reg [2:0]hold,

	// parameter	
	input [7:0]holdpos
);

	reg running;
	reg [7:0]state;
	
	assign la = running;
	
	always @(posedge clk or posedge res)
	begin
		if (res)
		begin
			state     <= 8'd0;
			cal_pulse <= 3'd0;
			hold      <= 3'd0;
		end
		else if (stop)
		begin
			state     <= 8'd0;
			cal_pulse <= 3'd0;
			hold      <= 3'd0;
		end
		else
		begin
			if (tin)
			begin
				state     <= 8'd1;
				cal_pulse <= 3'b00_1;
				hold      <= 3'b00_0;
			end
			else if (|state) state <= state + 8'd1;
			
			if (state ==  8'b10000010) cal_pulse <= 3'b00_0;
			if (state == {2'b10, holdpos[7:2]}) hold <= {holdpos[1:0], 1'b1}; 

		end
	end

	assign cal_ena = running;

	assign tout = (state == 8'd255);
	
	always @(posedge clk or posedge res)
	begin
		if (res) running <= 1'b0;
		else if (stop)   running <= 1'b0;
		else if (tin)    running <= 1'b1;
		else if (tout)   running <= 1'b0;
	end
	
endmodule
