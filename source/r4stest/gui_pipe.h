// pipe.h

#pragma once

#include <stdint.h>

class CPipeServerOut
{
    bool connected;
    int buffersize;
    int size;
    int pos;

    uint8_t *buffer;
   	HANDLE hPipe;

public:
    CPipeServerOut(int buffersize = 1024) : connected(false), buffersize(buffersize) {}
    ~CPipeServerOut() { Disconnect(); }

    void Flush();

    void WaitConnect(std::string name);
    void Disconnect();

    void Put_uint8(uint8_t x);
 
    void Put_uint16(uint16_t x)
    {
        Put_uint8((uint8_t)(x >> 8));
        Put_uint8((uint8_t)(x));
    }

    void Put_uint32(uint32_t x)
    {
        Put_uint16((uint16_t)(x >> 16));
        Put_uint16((uint16_t)(x));
    }

    void Put_int8(int8_t x)   { Put_uint8((int8_t)x); }
    void Put_int16(int16_t x) { Put_uint16((uint16_t)x); }
    void Put_int32(int32_t x) { Put_uint32((uint32_t)x); }
};



class CPipeServerIn
{
    bool connected;
    int buffersize;
    int size;
    int pos;

    uint8_t *buffer;

   	HANDLE hPipe;

    uint8_t FillBuffer();

public:
    CPipeServerIn(int buffersize = 1024) : connected(false), buffersize(buffersize) {}
    ~CPipeServerIn() { Disconnect(); }

    void WaitConnect(std::string name);
    void Disconnect();

    uint8_t Get_uint8();

    uint16_t Get_uint16()
    {
        uint16_t x = Get_uint8();
        return (uint16_t)((x << 8) + Get_uint8());
    }

    uint32_t Get_uint32()
    {
        uint32_t x = Get_uint16();
        return (x << 16) + Get_uint16();
    }

    int8_t  Get_int8()  { return (int8_t) Get_uint8(); }
    int16_t Get_int16() { return (int16_t)Get_uint16(); }
    int32_t Get_int32() { return (int32_t)Get_int32(); }
};


class CPipeServerInOut
{
	CPipeServerIn inPipe;
	CPipeServerOut outPipe;

public:
    CPipeServerInOut(int inBuffersize = 1024, int outBuffersize = 1024) {}
    ~CPipeServerInOut() { Disconnect(); }

    void WaitConnect(std::string name);
    void Disconnect();

	uint8_t Get_uint8()   { return inPipe.Get_uint8();  }
	uint16_t Get_uint16() { return inPipe.Get_uint16(); }
	uint32_t Get_uint32() { return inPipe.Get_uint32(); }
    int8_t  Get_int8()    { return inPipe.Get_int8();   }
    int16_t Get_int16()   { return inPipe.Get_int16();  }
    int32_t Get_int32()   { return inPipe.Get_int32();  }

	void Put_uint8(uint8_t x)   { outPipe.Put_uint8(x);  }
    void Put_uint16(uint16_t x) { outPipe.Put_uint16(x); }
    void Put_uint32(uint32_t x) { outPipe.Put_uint32(x); }
    void Put_int8(int8_t x)     { outPipe.Put_int8(x);   }
    void Put_int16(int16_t x)   { outPipe.Put_int16(x);  }
    void Put_int32(int32_t x)   { outPipe.Put_int32(x);  }
	void Flush() { outPipe.Flush(); }
};
