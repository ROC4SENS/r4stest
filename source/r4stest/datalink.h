// datalink.h

#pragma once

#include <string>
#include <stdint.h>
#include <vector>
#include "gui_pipe.h"


class CDataLink : public CPipeServerInOut
{
	uint16_t GetCmdHeader();
	void PutCmdHeader(uint16_t cmd);

	void Exec_SetExit();
	void Exec_SetCal();
	void Exec_SetVana();
	void Exec_GetID();
	void Exec_SetVdig();
	void Exec_GetIA();
	void Exec_SetVcal();
	void Exec_SetRgsh();
	void Exec_SetRgpr();
	void Exec_SetVref();
	void Exec_SetVaux1();
	void Exec_SetVaux2();
	void Exec_r4s_SetPixCal();
	void Exec_r4s_SetHoldPos();
	void Exec_r4s_AdcDelay();
	void Exec_GetImage();
	void Exec_r4s_SetSeqReadout();
	void Exec_r4s_SetSeqCalScan();
public:
	CDataLink(int inBuffersize = 1024, int outBuffersize = 1024)
		: CPipeServerInOut(inBuffersize, outBuffersize) {}

	void Run();
};
