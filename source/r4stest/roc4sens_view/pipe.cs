﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.IO.Pipes;


namespace roc4sens_view
{

class CPipeClientOut
{
    private bool connected;
    private int buffersize;
    private int size;

    private byte[] buffer;

    private NamedPipeClientStream pipe;

    public CPipeClientOut(int buffersize)
    {
        connected = false;
        this.buffersize = buffersize;
    }

    public CPipeClientOut() : this(1024) { }

    ~CPipeClientOut() { Disconnect(); }

    public void Flush()
    {
        if (size == 0) return;
        if (!connected) { size = 0; return; }
        pipe.Write(buffer, 0, size);
        size = 0;
    }

    public void Connect(string name)
    {
        Disconnect();
        pipe = new NamedPipeClientStream(".", name, PipeDirection.Out);
        pipe.Connect();

        connected = true;
        buffer = new byte[buffersize];
    }

    public void Disconnect()
    {
        if (connected)
        {
            pipe.Close();
            connected = false;
        }
    }

    public void Put_uint8(byte x)
    {
        buffer[size++] = x;
        if (size >= buffersize) Flush();
    }

    public void Put_uint16(ushort x)
    {
        Put_uint8((byte)(x >> 8));
        Put_uint8((byte)(x));
    }

    public void Put_uint32(uint x)
    {
        Put_uint16((ushort)(x >> 16));
        Put_uint16((ushort)(x));
    }

    public void Put_int8(sbyte x)
    {
        Put_uint8((byte)x);
    }

    public void Put_int16(short x)
    {
        Put_uint16((ushort)x);
    }

    public void Put_int32(int x)
    {
        Put_uint32((uint)x);
    }
}


class CPipeClientIn
{
    private bool connected;
    private int buffersize;
    private int size;
    private int pos;

    private byte[] buffer;

    private NamedPipeClientStream pipe;

    public CPipeClientIn(int buffersize)
    {
        connected = false;
        this.buffersize = buffersize;
    }

    public CPipeClientIn() : this(1024) { }

    ~CPipeClientIn() { Disconnect(); }

    private byte FillBuffer()
    {
        if (!connected) return 0;
        size = pipe.Read(buffer, 0, buffersize);
        pos = 0;
        if (size == 0) return 0;
        return buffer[pos++];
    }

    public void Connect(string name)
    {
        Disconnect();
        pipe = new NamedPipeClientStream(".", name, PipeDirection.In);
        pipe.Connect();

        connected = true;
        buffer = new byte[buffersize];
    }

    public void Disconnect()
    {
        if (connected)
        {
            pipe.Close();
            connected = false;
        }
    }

    public byte Get_uint8()
    {
        return (pos >= size) ? FillBuffer() : buffer[pos++];
    }

    public ushort Get_uint16()
    {
        uint x = Get_uint8();
        return (ushort)((x << 8) + Get_uint8());
    }

    public uint Get_uint32()
    {
        uint x = Get_uint16();
        return (x << 16) + Get_uint16();
    }

    public sbyte Get_int8()
    {
        return (sbyte)Get_uint8();
    }

    public short Get_int16()
    {
        return (short)Get_uint16();
    }

    public int Get_int32()
    {
        return (int)Get_int32();
    }
}


} // namespace
