﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace roc4sens_view
{

    class Dataprocessing
    {
        private int Cols;
        private int Rows;

        private bool first = true;
        private double[,] data;
        private bool darkActive = false;
        private double[,] dark;

        public Dataprocessing(int Cols, int Rows)
        {
            this.Cols = Cols;
            this.Rows = Rows;
            data = new double[Cols, Rows];
            dark = new double[Cols, Rows];
            average = 0;
            Reset();
        }

        public int average { set; get; }

        public void Reset()
        {
            first = true;
        }

        public double[,] SetDark()
        {
            for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++)
            {
                dark[x, y] = data[x, y];
                data[x, y] = 0;
            }
            darkActive = true;
            return data;
        }

        public double[,] ClearDark()
        {
            for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++)
            {
                data[x, y] = dark[x, y];
            }
            darkActive = false;
            return data;
        }

        public double[,] Calculate(double[,] din)
        {
            if (average == 0 || first)
            {
                if (darkActive)
                    for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++) data[x, y] = din[x, y] - dark[x, y];
                else
                    for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++) data[x, y] = din[x, y];
                first = false;
            }
            else
            {
                double k1 = average/50.0;
                double k2 = 1 - k1;
                if (darkActive)
                {
                    for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++)
                     {
                         data[x, y] = k1 * data[x, y] + k2 * (din[x, y] - dark[x, y]);
                     }
                }
                else
                {
                    for (int x = 0; x < Cols; x++) for (int y = 0; y < Rows; y++)
                            data[x, y] = k1 * data[x, y] + k2 * din[x, y];
                }
            }
            return data;
        }
    }

}
