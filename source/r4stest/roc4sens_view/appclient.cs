﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.IO.Pipes;


namespace roc4sens_view
{

public delegate void Callback_GetID(ushort id);
public delegate void Callback_GetIA(ushort ia);
public delegate void Callback_GetImage(short[] img);



class AppClient
{
    private const byte CMD_MARKER = 0xc0;

    CPipeClientOut pipeOut;
    CPipeClientIn pipeIn;

    private Thread thread;
    private bool running;

    private int GetCmdHeader()
    {
        byte marker = pipeIn.Get_uint8();
        if (marker != CMD_MARKER) return -1;
        return pipeIn.Get_uint16();
    }

    private void PutCmdHeader(ushort cmdId)
    {
        pipeOut.Put_uint8(CMD_MARKER);
        pipeOut.Put_uint16(cmdId);
    }


// =======================================================
    /*
          0: SetExit();
          1: SetVD(ushort mV)
          2: GetID()
          3: SetVA(ushort mV)
          4: GetIA()
         20: SetVcal(uint16_t mV)
         21: SetRgsh(uint16_t mV)
         22: SetRgpr(uint16_t mV)
         23: SetVref(uint16_t mV)
         24: r4s_SetPixCal(uint8_t x, uint8_t y)
         25: r4s_SetHoldPos(uint8_t t)
         26: r4s_AdcDelay(uint8_t t)
 	     30: SetVaux(uint16_t mV)
	     31: SetVaux(uint16_t mV)
         33: SetCal(uint8_t x, uint8_t y);
         34: r4s_SetSeqReadout();
         35: r4s_SetSeqCalScan();
        100: GetImage()

       1000: RecExit()
       1002: ushort ret GetId()
       1004: ushort ret GetIa()
       1100: {byte width, byte height, short[] rawImg} ret GetImage()
    */

    public void Start()
    {
        Thread.Sleep(500);
        pipeOut = new CPipeClientOut(128);
        pipeIn  = new CPipeClientIn(128);
        pipeOut.Connect("roc4sens_to");
        Thread.Sleep(500);
        pipeIn.Connect("roc4sens_from");

        thread = new Thread(Run);
        thread.Start();
        while (!thread.IsAlive);
        Thread.Sleep(1);
    }

    public void SetExit()
    {
        PutCmdHeader(0);
        pipeOut.Flush();
        thread.Abort();
    }
    
    public void SetCal(byte x, byte y)
    {
        PutCmdHeader(33);
        pipeOut.Put_uint8(x);
        pipeOut.Put_uint8(y);
        pipeOut.Flush();
    }

    public void SetVdig(ushort value)
    {
        PutCmdHeader(1);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void GetID()
    {
        PutCmdHeader(2);
        pipeOut.Flush();
    }

    public void SetVana(ushort value)
    {
        PutCmdHeader(3);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void GetIA()
    {
        PutCmdHeader(4);
        pipeOut.Flush();
    }

    public void SetVcal(ushort value)
    {
        PutCmdHeader(20);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void SetRgsh(ushort value)
    {
        PutCmdHeader(21);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void SetRgpr(ushort value)
    {
        PutCmdHeader(22);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void SetVref(ushort value)
    {
        PutCmdHeader(23);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void SetVaux1(ushort value)
    {
        PutCmdHeader(30);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }
    
    public void SetVaux2(ushort value)
    {
        PutCmdHeader(31);
        pipeOut.Put_uint16(value);
        pipeOut.Flush();
    }

    public void r4s_SetPixCal(byte x, byte y)
    {
        PutCmdHeader(24);
        pipeOut.Put_uint8(x);
        pipeOut.Put_uint8(y);
        pipeOut.Flush();
    }

    public void r4s_SetHoldPos(byte t)
    {
        PutCmdHeader(25);
        pipeOut.Put_uint8(t);
        pipeOut.Flush();
    }

    public void r4s_AdcDelay(byte t)
    {
        PutCmdHeader(26);
        pipeOut.Put_uint8(t);
        pipeOut.Flush();
    }
 
    public void r4s_SetSeqReadout()
    {
        PutCmdHeader(34);
        pipeOut.Flush();
    }

    public void r4s_SetSeqCalScan()
    {
        PutCmdHeader(35);
        pipeOut.Flush();
    }

    public void GetImage(bool slowReadout)
    {
        PutCmdHeader(100);
        pipeOut.Put_uint8((byte)(slowReadout ? 1 : 0));
        pipeOut.Flush();
    }


    private void Run()
    {
        running = true;
        while (running)
        {
            int cmd = GetCmdHeader();
            switch (cmd)
            {
                case 1000: running = false; break;
                case 1002: Ret_GetID(); break;
                case 1004: Ret_GetIA(); break;
                case 1100: Ret_GetImage(); break;
            }
        }
        pipeOut.Disconnect();
        pipeIn.Disconnect();
    }

    public Callback_GetID callback_GetID { private get; set; }
    public Callback_GetIA callback_GetIA { private get; set; }
    public Callback_GetImage callback_GetImage { private get; set; }

    private void Ret_GetID()
    {
        ushort id = pipeIn.Get_uint16();
        if (callback_GetID != null) callback_GetID(id);
    }

    private void Ret_GetIA()
    {
        ushort ia = pipeIn.Get_uint16();
        if (callback_GetIA != null) callback_GetIA(ia);
    }

    private void Ret_GetImage()
    {
        uint vectorSize = pipeIn.Get_uint32();
        if (vectorSize > 2000000) vectorSize = 2000000;
        short[] img = new short[vectorSize];
        for (int i=0; i<vectorSize; i++) img[i] = pipeIn.Get_int16();

        if (callback_GetImage != null) callback_GetImage(img);
    }
}

}
