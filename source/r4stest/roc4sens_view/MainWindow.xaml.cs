﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Microsoft.Win32;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;


namespace roc4sens_view
{


public delegate void ThreadStart();

/// <summary>
/// Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window, INotifyPropertyChanged
{
    public event PropertyChangedEventHandler PropertyChanged;
    private void Notify(string argument)
    {
        if (this.PropertyChanged != null)
        {
            this.PropertyChanged(this, new PropertyChangedEventArgs(argument));
        }
    }

    private DispatcherTimer timer;
    private DispatcherTimer dtbUpdateTimer;
    private AppClient pipe;

    public MainWindow()
    {
        InitializeComponent();
        this.DataContext = this;
        calPixel.Content = String.Format("Pixel ({0}/{1}):", _CalX, CalY);

        SlowReadout = true;
        CalScan = false;

        timer = new DispatcherTimer();
        timer.Tick += new EventHandler(timer_Tick);
        timer.Interval = new TimeSpan(0, 0, 0, 0, 100);

        dtbUpdateTimer = new DispatcherTimer();
        dtbUpdateTimer.Tick += new EventHandler(dtbUpdateTimer_Tick);
        dtbUpdateTimer.Interval = new TimeSpan(0, 0, 0, 0, 200);
        dtbUpdateTimer.Start();

        pipe = new AppClient();
        pipe.callback_GetID = Event_GetID;
        pipe.callback_GetIA = Event_GetIA;
        pipe.callback_GetImage = Event_GetImage;
        pipe.Start();
    }

    private void Event_GetID(ushort id)
    {
        double v = (double)id / 10.0;
        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
            (ThreadStart)delegate()
            {
                txtID.Content = v.ToString("##0.0");
            }
        );
    }

    private void Event_GetIA(ushort ia)
    {
        double v = (double)ia / 10.0;
        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
            (ThreadStart)delegate()
            {
                txtIA.Content = v.ToString("##0.0");
            }
        );
    }

    private int calDisplayCount = 0;
    private double calDisplayMean = 0.0;

    private void Event_GetImage(short[] rawimg)
    {
        this.Dispatcher.BeginInvoke(DispatcherPriority.Normal,
            (ThreadStart)delegate()
            {
                pixelmap.Event_GetImage(rawimg);

                // if (lowpass > 0)
                if (running)
                {
                    calDisplayCount++;
                    calDisplayMean += pixelmap.data[CalX, CalY];
                                     
                    if (calDisplayCount >= 15)
                    {
                        calPixelValue.Content = String.Format("{0:F1}", calDisplayMean/calDisplayCount);
                        calDisplayCount = 0;
                        calDisplayMean = 0.0;
                    }
                }
                else
                {
                    calDisplayCount = 1;
                    calDisplayMean = pixelmap.data[CalX, CalY];
                    calPixelValue.Content = String.Format("{0:F1}", calDisplayMean);
                }
            }
        );
    }


    private PixelMap pixelmap = new PixelMap();
    public PlotModel Model { get { return pixelmap.plot; } private set { pixelmap.plot = value; } }
    public double mapOffs
    {
        get { return pixelmap.mapOffs; }
        set { pixelmap.mapOffs = value; }
    }
    public double mapScale
    {
        get { return pixelmap.mapScale; }
        set { pixelmap.mapScale = value; }
    }


    public bool SlowReadout { get; set; }
    public int lowpass { get { return pixelmap.lowpass; } set { pixelmap.lowpass = value; Notify("lowpass"); } }

    // --- DTB settings
    private int    _CalX = 0;
    private bool   _CalX_changed = true;
    private int    _CalY = 0;
    private bool   _CalY_changed = true;
    private double _Vdig = 2.2;
    private bool   _Vdig_changed = true;
    private double _Vana = 2.0;
    private bool   _Vana_changed = true;
    private double _Vcal = 0.9;
    private bool   _Vcal_changed = true;
    private double _Voffs = 0.25;
    private bool   _Voffs_changed = true;
    private double _Vrgpr = 0.6;
    private bool   _Vrgpr_changed = true;
    private double _Vrgsh = 0.6;
    private bool   _Vrgsh_changed = true;
    private int    _HoldPos = 11;
    private bool   _HoldPos_changed = true;
    private int    _AdcSampling = 10;
    private bool   _AdcSampling_changed = true;

    public int CalX { get { return _CalX; } set { _CalX = value; _CalX_changed = true; Notify("CalX"); } }
    public int CalY { get { return _CalY; } set { _CalY = value; _CalY_changed = true; Notify("CalY"); } }
    public double Vdig { get { return _Vdig; } set { _Vdig = value; _Vdig_changed = true; Notify("Vdig"); } }
    public double Vana  { get { return _Vana; } set { _Vana = value; _Vana_changed = true; Notify("Vana"); } }
    public double Vcal  { get { return _Vcal; } set { _Vcal = value; _Vcal_changed = true; Notify("Vcal"); } }
    public double Voffs { get { return _Voffs; } set { _Voffs = value; _Voffs_changed = true; Notify("Voffs"); } }
    public double Vrgpr { get { return _Vrgpr; } set { _Vrgpr = value; _Vrgpr_changed = true; Notify("Vrgpr"); } }
    public double Vrgsh { get { return _Vrgsh; } set { _Vrgsh = value; _Vrgsh_changed = true; Notify("Vrgsh"); } }
    public int HoldPos { get { return _HoldPos; } set { _HoldPos = value; _HoldPos_changed = true; Notify("HoldPos"); } }
    public int AdcSampling
    {
        get { return _AdcSampling; }
        set
        {
            if (value != _AdcSampling) { _AdcSampling = value; _AdcSampling_changed = true; Notify("AdcSampling"); }
        }
    }

    private void dtbUpdateTimer_Tick(object sender, EventArgs e)
    {
        pipe.GetID();
        pipe.GetIA();
        if (_CalX_changed) { pipe.SetCal((byte)_CalX, (byte)_CalY); _CalX_changed = _CalY_changed = false; }
        if (_CalY_changed) { pipe.SetCal((byte)_CalX, (byte)_CalY); _CalY_changed = _CalY_changed = false; }
        if (_Vdig_changed) { pipe.SetVdig((ushort)(_Vdig * 1000)); _Vdig_changed = false; }
        if (_Vana_changed) { pipe.SetVana((ushort)(_Vana * 1000)); _Vana_changed = false; }
        if (_Vcal_changed) { pipe.SetVcal((ushort)(_Vcal * 1000)); _Vcal_changed = false; }
        if (_Voffs_changed) { pipe.SetVref((ushort)(_Voffs * 1000)); _Voffs_changed = false; }
        if (_Vrgpr_changed) { pipe.SetRgpr((ushort)(_Vrgpr * 1000)); _Vrgpr_changed = false; }
        if (_Vrgsh_changed) { pipe.SetRgsh((ushort)(_Vrgsh * 1000)); _Vrgsh_changed = false; }
        if (_HoldPos_changed) { pipe.r4s_SetHoldPos((byte)_HoldPos); _HoldPos_changed = false; }
        if (_AdcSampling_changed) { pipe.r4s_AdcDelay((byte)_AdcSampling); _AdcSampling_changed = false; }
    }



    private bool running = false;
    private bool isDark = false;

    private void btnSingle_Click(object sender, RoutedEventArgs e)
    {
        if (running)
        {
            timer.Stop();
            btnLoop.Content = "Loop";
            running = false;
        }
        pipe.GetImage(SlowReadout);
        // pixelmap.data = pixelmap.GenerateRandomData();
    }

    private void btnStart_Click(object sender, RoutedEventArgs e)
    {
        if (running)
        {
            timer.Stop();
            btnLoop.Content = "Loop";
            running = false;
        }
        else
        {
            timer.Start();
            btnLoop.Content = "Stop";
            running = true;
        }
    }
 
    private void timer_Tick(object sender, EventArgs e)
    {
        pipe.GetImage(SlowReadout);
        // pixelmap.data = pixelmap.GenerateRandomData();
    }

    private void btnExit_Click(object sender, RoutedEventArgs e)
    {
        Environment.Exit(0);
        // Application.Current.Shutdown();
    }

    private void btnReset_Click(object sender, RoutedEventArgs e)
    {
        pixelmap.ResetData();
    }

    private void btnSetDark_Click(object sender, RoutedEventArgs e)
    {
        if (isDark)
        {
            pixelmap.ClearDark();
            btnSetDark.Content = "Set Dark";
            isDark = false;
        }
        else
        {
            pixelmap.SetDark();
            btnSetDark.Content = "Clear Dark";
            isDark = true;
        }
    }

    private void btnSave_Click(object sender, RoutedEventArgs e)
    {
        SaveFileDialog saveFileDialog = new SaveFileDialog();
        if (saveFileDialog.ShowDialog() == true)
        {
            try
            {
                pixelmap.SaveImageData(saveFileDialog.FileName);
            }
            catch (Exception ex) { txtMessage.Text = ex.Message; }
        }
    }

    private void Cal_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
    {
        calPixel.Content = String.Format("Pixel ({0}/{1}):", _CalX, CalY);
        calPixelValue.Content = "";
        calDisplayCount = 0;
        calDisplayMean = 0.0;
    }


    public bool CalScan { get; set; }

    private void CalScan_On(object sender, RoutedEventArgs e)
    {
        txtCalX.IsEnabled = false;
        sldCalX.IsEnabled = false;
        txtCalY.IsEnabled = false;
        sldCalY.IsEnabled = false;

        pipe.r4s_SetSeqCalScan();
    }


    private void CalScan_Off(object sender, RoutedEventArgs e)
    {
        txtCalX.IsEnabled = true;
        sldCalX.IsEnabled = true;
        txtCalY.IsEnabled = true;
        sldCalY.IsEnabled = true;
        
        _CalX_changed = true;
        _CalY_changed = true;
        pipe.r4s_SetSeqReadout();    
    }

}


[ValueConversion(typeof(string), typeof(double))]
public class DoubleStringConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        return ((double)value).ToString("0.000");
    }

    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        double res;
        if (!Double.TryParse((string)value, out res)) return null;
        return res;
    }
}

} // namespace
