﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;


namespace roc4sens_view
{
    public class PixelMap
    {
        public const int Cols = 157;
        public const int Rows = 163;
        private Random rnd = new Random();

        private double colValue {get; set;}

        private Dataprocessing dataProc = new Dataprocessing(Cols, Rows);

        public double[,] GenerateRandomData()
        {
            var result = new double[Cols, Rows];

            for (var i = 0; i < Cols; i++)
            {
                for (var j = 0; j < Rows; j++)
                {
                    result[i, j] = rnd.Next(10, 600);
                }
            }
            return result;
        }

        public PlotModel plot;
        private LinearColorAxis scale;
        private HeatMapSeries plotData;

        public int lowpass { set { dataProc.average = value; } get { return dataProc.average; } }

        public void SetDark()   { data = dataProc.SetDark(); }
        public void ClearDark() { data = dataProc.ClearDark(); }
        public void ResetData() { dataProc.Reset(); }

        public void Event_GetImage(short[] rawimg)
        {
            if (rawimg.Length < Rows*Cols) return;

            double[,] img = new double[Cols, Rows];
            int x, y, pos = 0;
            for (y = 0; y < Rows; y++) for (x = 0; x < Cols; x++)
            {
                int value = rawimg[pos++];
                if ((value & 0x1000) != 0) // ADC overrange
                    value = -5000;
                else if ((value & 0x0800) != 0) // negative
                    value -= 0x1000;

                img[x, y] = value;
            }
            data = dataProc.Calculate(img);
        }

        /// <summary>
        /// ROC4sens settings
        /// </summary>

        // Pixel Map Data
        public double[,] data
        {
            get { return plotData.Data; }
            set
            {
                plotData.Data = value;
                plot.InvalidatePlot(true);
            }
        }

        private double mapOffsProp = 0;
        private double mapScaleProp = 1000;
  
        public double mapOffs
        {
            get { return mapOffsProp; }
            set
            {
                mapOffsProp = value;
                scale.Zoom(mapOffs-mapScale, mapOffs+mapScale);
                plot.InvalidatePlot(false);
            }
        }

        public double mapScale
        {
            get { return mapScaleProp; }
            set
            {
                mapScaleProp = value;
                scale.Zoom(mapOffs - mapScale, mapOffs + mapScale);
                plot.InvalidatePlot(false);
            }
        }

        public void SaveImageData(string filename)
        {
            // StreamWriter f = new StreamWriter(filename, 

            FileStream f = new FileStream(filename, FileMode.Create, FileAccess.Write);
            StreamWriter s = new StreamWriter(f);


            for (int y = Rows - 1; y >= 0; y--)
            {
                for (int x = 0; x < Cols; x++)
                    s.Write(String.Format(" {0,5:0}", data[x, y]));

                s.WriteLine();
            }
            f.Close();
        }

        public PixelMap()
        {
            colValue = 0.5;

            plot = new PlotModel();
            plot.Title = "Pixel Map";

            // --- color scale
            scale = new LinearColorAxis
            {
                Palette = OxyPalette.Interpolate(200,
                    OxyColor.FromRgb(255, 255, 200),
                    OxyColor.FromRgb(255, 240,  32),
                    OxyColor.FromRgb(100, 200,  60),  // OxyColor.FromRgb(255, 150, 100),
                    OxyColor.FromRgb( 16,  80, 150),
                    OxyColor.FromRgb(  0,  30,  60)
                ),

                // Palette = OxyPalettes.Jet(100), // .Cool(100), // Gray(100), // Rainbow(100),
                HighColor = OxyColors.Aqua,
                LowColor = OxyColors.Red,
                Position = AxisPosition.Right,
                MajorTickSize = 16,
                MinorTickSize = 6,
                AxisDistance = 8,
                AxisTitleDistance = 2,
                Title = "ADC value",
                TitleFontSize = 16,
                IsZoomEnabled = true
//                Minimum = 0,
//                Maximum = 600
            };
            scale.Zoom(mapOffs - mapScale, mapOffs + mapScale);
            plot.Axes.Add(scale);

            // --- x axis
            var axisX = new LinearAxis();
            axisX.Position = AxisPosition.Bottom;
            plot.Axes.Add(axisX);

            // --- y axis
            var axisY = new LinearAxis();
            plot.Axes.Add(axisY);

            plotData = new HeatMapSeries
            {
                X0 = 0,
                X1 = Cols-1,
                Y0 = 0,
                Y1 = Rows-1,
                Interpolate = false,
                RenderMethod = HeatMapRenderMethod.Bitmap,
                Data = GenerateRandomData()
            };
            plot.Series.Add(plotData);
        }
    }

}
