// pipe.cpp


#include "windows.h"
#include <string>
#include "gui_pipe.h"

using namespace std;


// --- CPipeServerOut -----------------------------------------------

void CPipeServerOut::Flush()
{
	if (size == 0) return;
	if (!connected) { size = 0; throw int(5); }
	DWORD dwWritten;
	WriteFile(hPipe, buffer, size, &dwWritten, NULL);
	size = 0;
}


void CPipeServerOut::WaitConnect(string name)
{
	Disconnect();

	string pipeName = "\\\\.\\pipe\\";
	pipeName += name;
	hPipe = CreateNamedPipe(pipeName.c_str(),
		PIPE_ACCESS_OUTBOUND,
		PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
		1,
		buffersize,
		0,
		NMPWAIT_USE_DEFAULT_WAIT,
		NULL);

	if (hPipe == INVALID_HANDLE_VALUE) throw int(1);

	connected = ConnectNamedPipe(hPipe, NULL) != FALSE;
	if (!connected) throw int(10);

	buffer = new uint8_t[buffersize];
	pos = size = 0;
}


void CPipeServerOut::Disconnect()
{
	if (connected)
	{
		DisconnectNamedPipe(hPipe);
		CloseHandle(hPipe);
		delete[] buffer;
		connected = false;
	}
}


void CPipeServerOut::Put_uint8(byte x)
{
    buffer[size++] = x;
    if (size >= buffersize) Flush();
}



// --- CPipeServerIn ------------------------------------------------

uint8_t CPipeServerIn::FillBuffer()
{
 	if (!connected) throw int(2);
	
	DWORD dwRead;
	do
	{
		if (!ReadFile(hPipe, buffer, buffersize, &dwRead, NULL))
		{
			unsigned int error = GetLastError();
			if (error == ERROR_BROKEN_PIPE) throw int(0);
			printf("\nGetLastError = %u", error);
			throw int(3);
		}
	} while (dwRead == 0);

	size = dwRead;
	pos = 0;
	if (size == 0) throw int(4);
	return buffer[pos++];
}


void CPipeServerIn::WaitConnect(string name)
{
	Disconnect();

	string pipeName = "\\\\.\\pipe\\";
	pipeName += name;
	hPipe = CreateNamedPipe(pipeName.c_str(),
		PIPE_ACCESS_INBOUND,
		PIPE_TYPE_BYTE | PIPE_READMODE_BYTE | PIPE_WAIT,
		1,
		0,
		buffersize,
		NMPWAIT_USE_DEFAULT_WAIT,
		NULL);

	if (hPipe == INVALID_HANDLE_VALUE) throw int(1);

	connected = ConnectNamedPipe(hPipe, NULL) != FALSE;
	if (!connected) throw int(10);

	buffer = new uint8_t[buffersize];
	pos = size = 0;
}


void CPipeServerIn::Disconnect()
{
	if (connected)
	{
		DisconnectNamedPipe(hPipe);
		CloseHandle(hPipe);
		delete[] buffer;
		connected = false;
	}
}


uint8_t CPipeServerIn::Get_uint8()
{
    return (pos >= size) ? FillBuffer() : buffer[pos++];
}


// --- CPipeServerInOut ---------------------------------------------

void CPipeServerInOut::WaitConnect(std::string name)
{
	printf("Wait for connection to (server <-- client) ...");
	inPipe.WaitConnect (name + "_to");
	printf(" connected\n");

	printf("Wait for connection from (server --> client) ...");
	outPipe.WaitConnect(name + "_from");
	printf(" connected\n");
}


void CPipeServerInOut::Disconnect()
{
	outPipe.Disconnect();
	inPipe.Disconnect();
}
