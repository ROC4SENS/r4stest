// datalink.cpp

#include "r4stest.h"
#include "datalink.h"


#define CMD_MARKER 0xc0


uint16_t CDataLink::GetCmdHeader()
{
	uint8_t hdr = Get_uint8();
	if (hdr != CMD_MARKER) throw int(6);
	return Get_uint16();
}


void CDataLink::PutCmdHeader(uint16_t cmd)
{
	Put_uint8(CMD_MARKER);
	Put_uint16(cmd);
}


// =======================================================
/*
     0: SetExit();
     1: SetVdig(ushort mV)
     2: GetID()
     3: SetVana(ushort mV)
     4: GetIA()
    20: SetVcal(uint16_t mV)
    21: SetRgsh(uint16_t mV)
    22: SetRgpr(uint16_t mV)
    23: SetVref(uint16_t mV)
    24: r4s_SetPixCal(uint8_t x, uint8_t y)
    25: r4s_SetHoldPos(uint8_t t)
    26: r4s_AdcDelay(uint8_t t)
	30: SetVaux1(uint16_t mV)
	31: SetVaux2(uint16_t mV)
    33: SetCal(uint8_t x, uint8_t y);
	34: r4s_SetSeqReadout();
	35: r4s_SetSeqCalScan();

   100: GetImage(bool slowReadout)

  1000: RecExit()
  1002: ushort ret GetId()
  1004: ushort ret GetIa()
  1100: {byte width, byte height, short[] rawImg} ret GetImage()
*/

void CDataLink::Exec_SetVdig()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVdig(value);
	tb.Flush();
}

void CDataLink::Exec_SetCal()
{
	uint8_t x = Get_uint8();
	uint8_t y = Get_uint8();
	tb.r4s_SetPixCal(x, y);
	tb.Flush();
}

void CDataLink::Exec_GetID()
{
	uint16_t value = tb._GetID();
	PutCmdHeader(1002);
	Put_uint16(value);
	tb.Flush();
}

void CDataLink::Exec_SetVana()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVana(value);
	tb.Flush();
}

void CDataLink::Exec_GetIA()
{
	uint16_t value = tb._GetIA();
	PutCmdHeader(1004);
	Put_uint16(value);
	tb.Flush();
}

void CDataLink::Exec_SetVcal()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVcal(value);
	tb.Flush();
}

void CDataLink::Exec_SetRgsh()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetRgsh(value);
	tb.Flush();
}

void CDataLink::Exec_SetRgpr()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetRgpr(value);
	tb.Flush();
}

void CDataLink::Exec_SetVref()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVref(value);
	tb.Flush();
}

void CDataLink::Exec_SetVaux1()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVaux1(value);
	tb.Flush();
}

void CDataLink::Exec_SetVaux2()
{
	uint16_t value = Get_uint16();
	tb.r4s_SetVaux2(value);
	tb.Flush();
}


void CDataLink::Exec_r4s_SetPixCal()
{
	uint8_t x = Get_uint8();
	uint8_t y = Get_uint8();
	tb.r4s_SetPixCal(x, y);
	tb.Flush();
}

void CDataLink::Exec_r4s_SetHoldPos()
{
	uint8_t t = Get_uint8();
	tb.r4s_SetHoldPos(t);
	tb.Flush();
}

void CDataLink::Exec_r4s_AdcDelay()
{
	uint8_t t = Get_uint8();
	tb.r4s_AdcDelay(t);
	tb.Flush();
}

void CDataLink::Exec_r4s_SetSeqReadout()
{
	tb.r4s_SetSeqReadout();
	tb.Flush();
}

void CDataLink::Exec_r4s_SetSeqCalScan()
{
	tb.r4s_SetSeqCalScan();
	tb.Flush();
}

void CDataLink::Exec_GetImage()
{
	uint8_t roMode = Get_uint8() ? 3 : 1;
	tb.Daq_Open(50000);

	// prepare ADC
	tb.SignalProbeADC(PROBEA_SDATA1, GAIN_1);
//	tb.r4s_AdcDelay(0);
	tb.r4s_Enable(roMode);
	tb.uDelay(100);

	// take data
	tb.Daq_Start();
	tb.r4s_Start();
	tb.uDelay(3000);
	tb.Daq_Stop();

	// stop ADC
	tb.r4s_Enable(0);

	// read buffer
	vector<uint16_t> data;
	unsigned int ret = tb.Daq_Read(data);

	tb.Daq_Close();
	tb.Flush();

	unsigned int n = data.size();
	PutCmdHeader(1100);
	Put_uint32(n);
	for (unsigned int i=0; i<n; i++) Put_uint16(data[i]);
	Flush();
}



void CDataLink::Run()
{
	WaitConnect("roc4sens");

	bool running = true;
	while (running)
	{
		try
		{
			uint16_t cmd = GetCmdHeader();
			switch (cmd)
			{
				case   0: running = false; break;
				case   1: Exec_SetVdig(); break;
				case   2: Exec_GetID(); break;
				case   3: Exec_SetVana(); break;
				case   4: Exec_GetIA(); break;
				case  20: Exec_SetVcal(); break;
				case  21: Exec_SetRgsh(); break;
				case  22: Exec_SetRgpr(); break;
				case  23: Exec_SetVref(); break;
				case  24: Exec_r4s_SetPixCal(); break;
				case  25: Exec_r4s_SetHoldPos(); break;
				case  26: Exec_r4s_AdcDelay(); break;
				case  30: Exec_SetVaux1(); break;
				case  31: Exec_SetVaux2(); break;
				case  33: Exec_SetCal(); break;
				case  34: Exec_r4s_SetSeqReadout(); break;
				case  35: Exec_r4s_SetSeqCalScan(); break;
				case 100: Exec_GetImage(); break;
			}
		}
		catch (int e)
		{
			if (e == 0)
			{
				printf("Client disconnected\n");
				running = 0;
			}
			else
			printf("\nERROR: %i\n", e);
		}
	}

	Disconnect();
}
