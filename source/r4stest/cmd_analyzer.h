/* -------------------------------------------------------------
 *
 *  file:        cmd_analyzer.cpp
 *
 *  description: command line interpreter
 *               experimental function
 *
 *  author:      Beat Meier
 *  modified:    25.7.2017
 *
 *  rev:
 *
 * -------------------------------------------------------------
 */

HELP_CAT("ana")
CMD_REG(getimg, "", "Read an image from r4s")
CMD_REG(seqreadout, "", "Load measure -> readout sequence")
CMD_REG(seqcalscan, "", "Load calibrate scan sequence")

//analysis
//CMD_REG(test, "" , "print 'Hello World!'")
CMD_REG(pixelAlive, "" ,"performs a pixel alive test")
CMD_REG(pixelDist,"<iterations>","distribution of calibration pulses per pixel")
CMD_REG(pulseShape,"<x> <y>","analogue out as a function of the hold position, creating a tree ")
CMD_REG(pulseShapeGraph,"<x> <y> <Rgpr><Rgsh> <Va> <Vcal>","analogue out as a function of the hold position. Rgsh, Rgpr and Va can be set. Output is a graph created  in a rootfile")
CMD_REG(pulseShapeRgsh, "<x> <y> <Rgpr><Rgsh min><Rgsh max><Rgsh step> <Va> <Vcal>","returns pulse shape graphs for Rgsh scanned in a root file")
//CMD_REG(gui, "", "Start graphical user interface");
