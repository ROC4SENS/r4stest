/* -------------------------------------------------------------
 *
 *  file:        command.cpp
 *
 *  description: command line interpreter for Chip/Wafer tester
 *
 *  author:      Beat Meier
 *  modified:    31.8.2007
 *
 *  rev:         Tilman Rohe
 *  dat:         22.08.2017 
 *
 * -------------------------------------------------------------
 */


#include "cmd.h"
#include "datalink.h"
#include "R4sImg.h"
#include "R4sLine.h"

//#include "pixelAlive.h"
#include "TROOT.h"
#include "TH1D.h"
#include "TH2D.h"
#include "TH2F.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"
#include "TLeaf.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TCanvas.h"




//================================================================================


bool ReadImage(R4sImg &map, bool verbose=true)
{
	tb.Daq_Open(50000);

	// prepare ADC
	tb.SignalProbeADC(PROBEA_SDATA1, GAIN_1);
//	tb.r4s_AdcDelay(0);
	tb.r4s_Enable(3); // slow readout
	tb.uDelay(100);

	// take data
	tb.Daq_Start();
	tb.r4s_Start();
	tb.uDelay(3000);
	tb.Daq_Stop();

	// stop ADC
	tb.r4s_Enable(0);

	// read buffer
	vector<uint16_t> data;
	unsigned int ret = tb.Daq_Read(data);

	unsigned int n = data.size();
	if (verbose) {printf("--- status = %u; n = %u\n", ret, (unsigned int)(data.size()));}

	tb.Daq_Close();

        return map.CreateRaw(data);
}


bool ReadLine(R4sLine &pixLine, bool verbose=true)
{
	tb.Daq_Open(50000);

	// prepare ADC
	tb.SignalProbeADC(PROBEA_SDATA1, GAIN_1);
//	tb.r4s_AdcDelay(0);
	tb.r4s_Enable(3); // slow readout
	tb.uDelay(100);

	// take data
	tb.Daq_Start();
	tb.r4s_Start();
	tb.uDelay(100);
	tb.Daq_Stop();

	// stop ADC
	tb.r4s_Enable(0);

	// read buffer
	vector<uint16_t> data;
	unsigned int ret = tb.Daq_Read(data);

	unsigned int n = data.size();
	if (verbose) {printf("--- status = %u; n = %u\n", ret, (unsigned int)(data.size()));}

	tb.Daq_Close();

        return pixLine.CreateRaw(data);
}

TGraph* CreatePulseShapeGraph(int calX,int calY, int Rgpr, int Rgsh, int Va, int Vcal){
 
 
  // create arrays with values
  int t[256], pulseHeight[256], ped[IMG_WIDTH]={0};// IMG_WIDTH=157
  int common_mode_correction;
  
  R4sLine line;

  const unsigned int hold_max = 255;
  const int ped_measurements = 100;
  const int ph_measurements = 10;

  //set parameters 
  tb._SetVA(Va);
  int Ia=tb.GetIA()*1000;
  printf("Set Va to %d mV, Ia is: %d mA\n",Va,Ia);
  tb.r4s_SetRgsh(Rgsh);
  tb.r4s_SetRgpr(Rgpr);
  printf("Set Rgpr to %d and Rgsh to %d mV\n",Rgpr,Rgsh);
  tb.r4s_SetPixCal(calX, calY);
  printf("Set cal for pixel x:%d y:%d\n",calX,calY);
  
  // set readoutsequence to read a line only
  tb.r4s_SetSeqReadLine();

  // measure pedestals

  printf("Measureing pedestals...\n");fflush(stdout);
  
  tb.r4s_SetVcal(0);
  tb.r4s_SetHoldPos(0);
  
  for (unsigned int i=0;i<ped_measurements;i++)
  {
      ReadLine(line, false);
      for (unsigned int j=0; j<IMG_WIDTH; j++) ped[j] += line.Get(j);
  }  

  for (unsigned int i=0; i<IMG_WIDTH; i++) ped[i] /= ped_measurements;
  
  //printf("pedestals: ");
  //for (unsigned int i=0; i<IMG_WIDTH; i++) printf(" %d ", ped[i]);
  //printf("\n\n");

  // take data

  tb.r4s_SetVcal(Vcal);
  printf("Set Vcal to %d mA\n\n",Vcal);

  printf("Taking data...\n");fflush(stdout);
  
  // loop over all values for hold
  for (unsigned int i=0;i<hold_max;i++)
    {
      tb.r4s_SetHoldPos(i);
            
      for (unsigned int j=0;j<ph_measurements;j++)
      {
        ReadLine(line, false);
      
        if (calX < 2) common_mode_correction = line.Get(calX+3) - ped[calX+3];
        else if (calX > 152) common_mode_correction = line.Get(calX-3) - ped[calX-3];
        else common_mode_correction = 0.5 * (line.Get(calX-3) - ped[calX-3] + line.Get(calX+3) - ped[calX+3]);  

        if (j==0){ 
            pulseHeight[i] = line.Get(calX) - ped[calX] - common_mode_correction;
        }
        else{
            pulseHeight[i] += line.Get(calX) - ped[calX] - common_mode_correction;
        }
      }
      //write values into arrays
      t[i] =  (i*6.25);//one unit in hold is 6.25ns
      pulseHeight[i] /= ph_measurements;
 
    }
    
  printf("Done.\n");  
 
  // t, pulseHeight arrays
  TGraph *gr = new TGraph( (hold_max), t, pulseHeight);   
  
  return gr;
    
}


//
//================================================================================


bool configure() {

  //need to have a function configuring the chip from a file
}

CMD_PROC(getimg)
{
	R4sImg map;
	ReadImage(map);
	map.Print(200);
	map.Save("map.txt");
}


CMD_PROC(seqreadout)
{
	tb.r4s_SetSeqReadout();
	DO_FLUSH
}


CMD_PROC(seqcalscan)
{
	tb.r4s_SetSeqCalScan();
	DO_FLUSH
}


// CMD_PROC(gui)
// {
// 	printf("Connect to GUI ...");
// 	CDataLink link(1024, 65536);
// //	ShellExecute(NULL, "open", "file.exe", NULL, NULL, SW_SHOWDEFAULT);

// 	system ("start roc4sens_view\\bin\\Release\\roc4sens_view.exe");
// //	system ("start roc4sens_view.exe");

// 	link.Run();
// 	printf("Connection to GUI closed.\n");
// }


//================================================================================

////////// ANALYSIS


CMD_PROC(pixelAlive)
{
  R4sImg map;
  ReadImage(map);
  map.Print(200);

  TFile *rootFile = new TFile("data/pixelAlive.root","recreate");

  TH2D *pixelMap = new TH2D("pixelMap","pixel map",IMG_WIDTH,0,IMG_WIDTH,IMG_HEIGHT,0,IMG_HEIGHT);
  TH1D *h = new TH1D("h","Distribution",120,-600,600);

  //debugging
  FILE *f = fopen("debug.txt", "wt");
  
  std::vector<int> data = map.getData();
  printf("data size: %d // should be %d \n",data.size(),IMG_HEIGHT*IMG_WIDTH);

  for (unsigned int i=0;i<data.size();i++)
    {
//       fprintf(f,"DEBUG: Filling %d : %d/%d \n",i,i%IMG_WIDTH,i/IMG_WIDTH);
      pixelMap->Fill(i%IMG_WIDTH,i/IMG_WIDTH,data[i]);
      h->Fill(data[i]);
    }
  fclose(f);

  // int *i;
  // char **ch;
  // TApplication app("app",i,ch);
  // TCanvas *c = new TCanvas("c","canvas",200,100);
  // h->Draw();
  // app.Run();
  pixelMap->Write();
  h->Write();
  rootFile->Close();
  
}

//pixel Distribution
//Distribution of adc values for a given Vcal per pixel
CMD_PROC(pixelDist) {

  int iterations;
  PAR_INT(iterations, 0, 2147483647) //INT_MAX

  TFile *rootFile = new TFile("pixelDist.root","recreate");
  TTree *tPixDist = new TTree("pixelDist","pixelDist");
  int col(0),row(0),iteration(0),value(0);
  tPixDist->Branch("col",&col,"col/I");
  tPixDist->Branch("row",&row,"row/I");
  tPixDist->Branch("it",&iteration,"it/I");
  tPixDist->Branch("value",&value,"value/I");

  std::vector< std::vector<int> > data;
  R4sImg map;
  
  //get the data
  printf("Taking data...");fflush(stdout);
  for (int i=0;i<iterations;i++)
    {
      if (!ReadImage(map,false)) {break;}
      std::vector<int> data_single = map.getData();
      printf("Iteration %d: data size: %d // should be %d \n",i+1,data_single.size(),IMG_HEIGHT*IMG_WIDTH);
      data.push_back(data_single);
    }
  printf("Done.\n");

  //filling the data into the tree
  printf("Writing the data into a TTree...");fflush(stdout);
  for (int j=0;j<IMG_HEIGHT*IMG_WIDTH;j++)
    {
      for (unsigned int k=0;k<data.size();k++)
	{
	  col = j%IMG_WIDTH;
	  row = j/IMG_WIDTH;
	  iteration = k;
	  value = data[k][j];
	  tPixDist->Fill();
	}
    }
  printf("Done.\n");

  tPixDist->Write();
  rootFile->Close();
}


//pulse shape analysis
CMD_PROC(pulseShape) {

  int calX,calY;
  PAR_INT(calX,0,255)
  PAR_INT(calY,0,255)

  TFile *rootFile = new TFile("pulseShape.root","recreate");
  TTree *tPulseShape = new TTree("pulseShape","pulseShape");
  int col(0),row(0),hold(0),value(0);
  tPulseShape->Branch("col",&col,"col/I");
  tPulseShape->Branch("row",&row,"row/I");
  tPulseShape->Branch("hold",&hold,"hold/I");
  tPulseShape->Branch("value",&value,"value/I");

  std::vector< std::vector<int> > data;
  R4sImg map;

  const unsigned int hold_max = 255;

  //get the data
  printf("Setting cal for pixel x:%d y:%d\n",calX,calY);
  tb.r4s_SetPixCal(calX, calY);
  printf("Taking data...");fflush(stdout);
  
  for (unsigned int i=0;i<hold_max;i++)
    {
      tb.r4s_SetHoldPos(i);
      if (!ReadImage(map,false)) {break;}
      std::vector<int> data_single = map.getData();
      printf("Hold %d: data size: %d // should be %d \n",i+1,data_single.size(),IMG_HEIGHT*IMG_WIDTH);
      data.push_back(data_single);
    }
  printf("Done.\n");

  //filling the data into the tree
  printf("Writing the data into a TTree...");fflush(stdout);
  for (int j=0;j<IMG_HEIGHT*IMG_WIDTH;j++)
    {
      for (unsigned int k=0;k<data.size();k++)
	{
	  col = j%IMG_WIDTH;
	  row = j/IMG_WIDTH;
	  hold = k;
	  value = data[k][j];
	  tPulseShape->Fill();
	}
    }
  printf("Done.\n");

  tPulseShape->Write();
  rootFile->Close();

}


//pulse shape analysis
//As it is, it takes too long...
// CMD_PROC(pulseShapeAll) {

//   TFile *rootFile = new TFile("pulseShapeAll.root","recreate");
//   TTree *tPulseShape = new TTree("pulseShape","pulseShape");
//   int col(0),row(0),hold(0),value(0);
//   tPulseShape->Branch("col",&col,"col/I");
//   tPulseShape->Branch("row",&row,"row/I");
//   tPulseShape->Branch("hold",&hold,"hold/I");
//   tPulseShape->Branch("value",&value,"value/I");

//   std::vector< std::vector<int> > data;
//   R4sImg map;

//   const unsigned int hold_max = 255;

//   //get the data
//   printf("Taking data...");fflush(stdout);
//   for (int j=0;j<IMG_HEIGHT*IMG_WIDTH;j++)
//     {
//       tb.r4s_SetPixCal(j%IMG_WIDTH, j/IMG_WIDTH);
//       std::vector<int> readings;
//       printf("pixel %d\n",j);
//       for (unsigned int i=0;i<hold_max;i++)
// 	{
// 	  tb.r4s_SetHoldPos(i);
// 	  if (!ReadImage(map,false)) {break;}
// 	  std::vector<int> data_single = map.getData();
// 	  int reading = data_single[j];
// 	  readings.push_back(reading);
// 	}
//       data.push_back(readings);
//     }
//   printf("Done.\n");
      

//   //filling the data into the tree
//   printf("Writing the data into a TTree...");fflush(stdout);
//   for (int j=0;j<IMG_HEIGHT*IMG_WIDTH;j++)
//     {
//       for (unsigned int k=0;k<data[j].size();k++)
// 	{
// 	  col = j%IMG_WIDTH;
// 	  row = j/IMG_WIDTH;
// 	  hold = k;
// 	  value = data[j][k];
// 	  tPulseShape->Fill();
// 	}
//     }
//   printf("Done.\n");

//   tPulseShape->Write();
//   rootFile->Close();

// }

//pulse shape, creating a graph
CMD_PROC(pulseShapeGraph) {
  int calX,calY, Rgpr, Rgsh, Va, Vcal;
  PAR_INT(calX,0,255);
  PAR_INT(calY,0,255);
  PAR_INT(Rgpr,0,2500);
  PAR_INT(Rgsh,0,2500);
  PAR_INT(Va,0,4000);
  PAR_INT(Vcal,0,2500);

  TFile *rootFile = new TFile("data/pulseShapeGraph.root","update");
 
 
  // t, pulseHeight arrays
  TGraph *gr;
  gr = CreatePulseShapeGraph(calX,calY,Rgpr,Rgsh,Va,Vcal);
  
  char GraphName[60];
  sprintf(GraphName, "PulseShapeX%dY%dRgpr%dRgsh%dVa%dVcal%d",calX,calY,Rgpr,Rgsh,Va,Vcal);
  gr->Write(GraphName);
  rootFile->Close();
  
}

CMD_PROC(pulseShapeRgsh) {
  int calX,calY, Rgpr, Rgsh_min, Rgsh_max, Rgsh_step, Va, Vcal;
  PAR_INT(calX,0,255);
  PAR_INT(calY,0,255);
  PAR_INT(Rgpr,0,2500);
  PAR_INT(Rgsh_min,0,2500);
  PAR_INT(Rgsh_max,0,2500);
  PAR_INT(Rgsh_step,0,2500);
  PAR_INT(Va,0,4000);
  PAR_INT(Vcal,0,2500);    
  
  if (Rgsh_max <= Rgsh_min){
   
      printf("Rgsh_max <= Rgsh_min, values swopped:\n");
      int help = Rgsh_max; Rgsh_max = Rgsh_min; Rgsh_min = help;
      
  }
  
  
  // Calculate number of steps
  if (Rgsh_step > (Rgsh_max-Rgsh_min)) Rgsh_step == (Rgsh_max-Rgsh_min);
  int nSteps = int((Rgsh_max-Rgsh_min)/Rgsh_step) + 1;
  printf("Rgsh_min: %d, Rgsh_max: %d, Rgsh_step: %d, number of measurements: %d\n",Rgsh_min,Rgsh_max,Rgsh_step,nSteps);
    
  TMultiGraph *mg = new TMultiGraph();
  TLegend *leg;
  TGraph *gr[nSteps];
  TFile *rootFile = new TFile("data/pulseShapeGraph.root","update");
  //TCanvas *c1;  
  //TH2F *hpx;
  
  char GraphName[80], mgName[80];
  
  for (int i=0; i<nSteps; i++){
      
      int Rgsh =  Rgsh_min + (i*Rgsh_step);     
      gr[i]=CreatePulseShapeGraph(calX,calY,Rgpr,Rgsh,Va,Vcal);
      mg->Add(gr[i]);

      sprintf(GraphName, "PulseShapeX%dY%dRgpr%dRgsh%dVa%dVcal%d",calX,calY,Rgpr,Rgsh,Va,Vcal);
      gr[i]->Write(GraphName);
      
  }

  sprintf(mgName, "PulseShapeX%dY%dRgpr%dRgsh%dto%dVa%dVcal%d",calX,calY,Rgpr,Rgsh_min,Rgsh_max,Va,Vcal);
  mg -> Write(mgName);
  rootFile->Close();
    
}




