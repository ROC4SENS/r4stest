ROC4SENS
========
***
Currently, the r4s project is being set up. Please, be patient and check in another time...
***

[TOC]

# Installation
***
## Linux:
***
### Prerequisites
* [FTD2XX driver](http://www.ftdichip.com/Drivers/D2XX.htm "FTD2XX driver for windows"). There must be a LD_LIBRARY_PATH variable containing the /usr/local/lib folder.

* libusb `sudo apt-get install libusb-1.0-0-dev`

* readline `sudo apt-get install libreadline6 libreadline6-dev`

***
>`cd r4stest/source/r4stest`

>`make`


## Windows:
***
### Prerequisites
* [FTD2XX driver](http://www.ftdichip.com/Drivers/D2XX.htm "FTD2XX driver for windows")

***
exists, but must be documented...

# Usage:
>`cd source/r4stest`

>`bin/r4stest LOG_FILE_NAME`


The `LOG_FILE_NAME` will overwrite the file if it exists.

## DTB:
***
In order to make the system recognise the DTB and set the appropriate permissions, create a file in `/etc/udev/rules.d/60-pixel_DTB.rules` which should contain:

>`SUBSYSTEM=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6014", MODE="0666"`

where the attribute values can be checked using `lsusb -v`